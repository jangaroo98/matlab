figure()
hold on
plot(PPWs,iters_jacobi(:,1),'o-')
plot(PPWs,iters_gs(:,1),'o-')
title('Iterations for two obstacles')
legend('Jacobi Iteration','Gauss-Seidel Iteration')
hold off 

figure()
hold on
plot(PPWs,iters_jacobi(:,2),'o-')
plot(PPWs,iters_gs(:,2),'o-')
title('Iterations for three obstacles')
legend('Jacobi Iteration','Gauss-Seidel Iteration')
hold off 

figure()
hold on
plot(PPWs,times_jacobi(1,:,1),'o-')
plot(PPWs,times_gs(1,:,1),'o-')
title('Convergence times for two obstacles')
legend('Jacobi Iteration','Gauss-Seidel Iteration')
hold off

figure()
hold on
plot(PPWs,times_jacobi(1,:,2),'o-')
plot(PPWs,times_gs(1,:,2),'o-')
title('Convergence times for three obstacles')
legend('Jacobi Iteration','Gauss-Seidel Iteration')
hold off

