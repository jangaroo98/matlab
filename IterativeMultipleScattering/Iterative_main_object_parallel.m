%Iterative_main_copy


%define a lot of importand constants, as well as set the obstacle and
%centers data. 
k = 2*pi;
PPW = 40;
phi = -pi/2;
z = 0;
L=8;
maxiter = 200;
tol = 1e-5;

% %for 3 objects
% num_obstacles = 3;
% obstacles = ['annu2' ; 'annu2' ; 'annu2'];
% r0s = [1 1 1];
% rmaxs = [2 2 2];
% centers = [-2.5 0 ; 2.5 0 ; 7.5 0];

% %for 4 objects
% num_obstacles = 4;
% obstacles = ['annu2' ; 'annu2' ; 'annu2' ; 'annu2'];
% r0s = [1 1 1 1];
% rmaxs = [2 2 2 2];
% centers = [-7.5 0 ; -2.5 0 ; 2.5 0 ; 7.5 0];

% %for 2 objects non-circle
% num_obstacles = 2;
% obstacles = ['roid3' ; 'roid3'];
% r0s = [0.5002 0.5002];
% rmaxs = [3 3];
% centers = [-4 0 ; 4 0];

%for 2 objects
num_obstacles = 2;
obstacles = ['annu2' ; 'annu2'];
r0s = [1 1];
rmaxs = [2 2];
% %vertical and coming from the right
% % centers = [0 -2.5 ; 0 2.5];
% % uinc = @(x,y) exp(1j*k*-x); %incident wave

% %horizontal and coming from the top
centers = [-2.5 0 ; 2.5 0];
uinc = @(x,y) exp(1j*k*(x*cos(phi) + y*sin(phi))); %incident wave


Xdisplaced = cell(num_obstacles,1);
Ydisplaced = cell(num_obstacles,1);
Xgrids = cell(num_obstacles,1);
Ygrids = cell(num_obstacles,1);
U = cell(num_obstacles,1);
A = cell(num_obstacles,1);



parfor i=1:num_obstacles
    r0 = r0s(i);
    rmax = rmaxs(i);
    N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
    N1s(i) = N1;
    T{i} = linspace(0,2*pi,N1);
    N2 = ceil(PPW*(rmax-r0)*k/(2*pi));
    N2s(i) = N2;
    
    fprintf(['\nGenerating grid for obstacle ' num2str(i) '\n'])
    [X,Y] = elliptic_polar(N1,N2,1.7,1e-5,10000,obstacles(i,:)); %generate grid
    
    Xgrids{i} = X;
    Ygrids{i} = Y;
    Xdisplaced{i} = X + centers(i,1);
    Ydisplaced{i} = Y + centers(i,2);
    
    U{i} = zeros(N1,N2);
   
end


parfor i=1:num_obstacles
    %get the matrix for each obstacle and the incident wave for each
    %obstacle
    A{i} = Helmholts_curvilinear_coefficient_matrix(Xgrids{i},Ygrids{i},L,k,z);
    incident{i} = uinc(Xdisplaced{i},Ydisplaced{i});
    
    for j=1:num_obstacles
        %i,j gives from center i to obstacle j
        if i~=j
            relativeX = Xdisplaced{j} - centers(i,1);
            relativeY = Ydisplaced{j} - centers(i,2);
            [radius{i,j},angles{i,j}] = topolar(relativeX,relativeY);
        end
    end
end

F = cell(num_obstacles,1);
G = cell(num_obstacles,1);

figure()
title(['iteration = ' num2str(1)])
parfor i=1:num_obstacles
    ls = 0:L-1;
    N1 = N1s(i);
    N2 = N2s(i);
    len = (N1 - 1)*(N2-1 + 2*L);
    b = zeros(len,1);
    b(1:N1-1) = -incident{i}(1:N1-1,1); %set b vector
    u = A{i}\b; %solve the system

    u_vec = u(1:(N1-1)*(N2-1));
    u_vec = reshape(u_vec,N1-1,[]);
    u_vec = [u_vec ; u_vec(1,:)];
    far = u((N1-1)*(N2-1) + 1:len);
    far = reshape(far,N1-1,2*L);
    far = [far ; far(1,1:2*L)];
    f = far(1:N1,1:2:2*L);
    g = far(1:N1,2:2:2*L);
    ls = 0:L-1;
    kR = k*rmaxs(i);
    broadcast = arrayfun(@(l) (kR)^(-l),ls);

    H0 = besselh(0,kR);
    H1 = besselh(1,kR);

    UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);
    uscat = [u_vec UN2]; 

    U{i} = uscat;
    F{i} = f;
    G{i} = g;
end

order = 1:num_obstacles;
for q=2:maxiter
    iteration = q;
    clf
    title(['plot of scattered wave, iteration = ' num2str(q)])
    F_new = cell(num_obstacles,1);
    G_new = cell(num_obstacles,1);
    hold on
    
    err = zeros(num_obstacles,1);
    parfor i=1:num_obstacles
        ls = 0:L-1;
        N1 = N1s(i);
        N2 = N2s(i);
        ms = 1:N1-1;
        len = (N1 - 1)*(N2-1 + 2*L);
        b = zeros(len,1);
        
        usc_other_obstacles = zeros(N1-1,1); 
        
        for j=1:num_obstacles
            if i~=j
                fj = F{j};
                gj = G{j};
                tj = T{j};
%                 for m=1:N1-1
                for l=ls
                    fjl = fj(1:N1,l+1);
                    gjl = gj(1:N1,l+1);
                    
                    fjl_interp = griddedInterpolant(tj,fjl,'spline');
                    gjl_interp = griddedInterpolant(tj,gjl,'spline');
                    
                    theta = angles{j,i}(1:N1,1);
                    r = radius{j,i}(1:N1,1);
                    
                    v = arrayfun(@(m) besselh(0,k*r(m)) * fjl_interp(theta(m)) * (k*r(m))^(-l),ms) ...
                        + arrayfun(@(m) besselh(1,k*r(m)) * gjl_interp(theta(m)) * (k*r(m))^(-l),ms);
                    v = reshape(v,N1-1,1);

                    usc_other_obstacles(1:N1-1) = usc_other_obstacles(1:N1-1) + v;
                    
                    
                end
            end
        end
        
        
        b(1:N1-1) = -incident{i}(1:N1-1,1) - usc_other_obstacles; %set b vector
        u = A{i}\b; %solve the system
        
        u_vec = u(1:(N1-1)*(N2-1));
        u_vec = reshape(u_vec,N1-1,[]);
        u_vec = [u_vec ; u_vec(1,:)];
        far = u((N1-1)*(N2-1) + 1:len);
        far = reshape(far,N1-1,2*L);
        far = [far ; far(1,1:2*L)];
        f = far(1:N1,1:2:2*L);
        g = far(1:N1,2:2:2*L);
        
        F_new{i} = f;
        G_new{i} = g;
        
        
        ls = 0:L-1;
        kR = k*rmaxs(i);
        broadcast = arrayfun(@(l) (kR)^(-l),ls);
        
        H0 = besselh(0,kR);
        H1 = besselh(1,kR);
        
        UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);
        uscat = [u_vec UN2]; 
        
        err(i) = norm(U{i} - uscat,'fro'); %check for convergence
        U{i} = uscat;
        
        %plot the scattered wave
        
        
    end
    hold on
    for i=1:num_obstacles
        surf(Xdisplaced{i},Ydisplaced{i},abs(U{i}))
    end
    axis equal
    view(2);
    hold off
    pause(0.1);
    
    
    F = F_new;
    G = G_new;
    

    if sum(err) < tol
        break;
    end
    

%     order =  [order(2:num_obstacles) order(1)];

end

%construct final image
figure()
clf
hold on

%this next portion calculates the total wave for the whole thing. Very
%expensive because I have to use the interpolating functions on every grid
%point. This is where most of the time is eaten up


fprintf(['\n\nConstructing total wave for all obstacles\n'])
tic;

parfor i=1:num_obstacles
    ls = 0:L-1;
    N1 = N1s(i);
    N2 = N2s(i);
    ms = 1:N1-1;
    len = (N1 - 1)*(N2-1 + 2*L);
    b = zeros(len,1);

    usc_other_obstacles = zeros(N1,N2); 

    for j=1:num_obstacles
        if i~=j
            fj = F{j};
            gj = G{j};
            tj = T{j};
            for l=ls
                fjl = fj(1:N1,l+1);
                gjl = gj(1:N1,l+1);

                fjl_interp = griddedInterpolant(tj,fjl,'spline');
                gjl_interp = griddedInterpolant(tj,gjl,'spline');

                theta = angles{j,i};
                r = radius{j,i};
                for m=1:N1
                    for n=1:N2

                        v = besselh(0,k*r(m,n)) * fjl_interp(theta(m,n)) * (k*r(m))^(-l) ...
                            + besselh(1,k*r(m,n)) * gjl_interp(theta(m,n)) * (k*r(m,n))^(-l);
                        usc_other_obstacles(m,n) = usc_other_obstacles(m,n) + v;
                    end
                end

            end
        end
    end
    Utot{i} = incident{i} + U{i} + usc_other_obstacles;
end
      
hold on
for i=1:num_obstacles
    surface(Xdisplaced{i},Ydisplaced{i},abs(Utot{i}))
end
    
toc
title('final construction');
view(2);
axis equal
hold off
