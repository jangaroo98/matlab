%Iterative_main


%define a lot of importand constants, as well as set the obstacle and
%centers data. 
k = 2*pi;
PPW = 40;

L=8;
maxiter = 200;
tol = 1e-5;

% %for 3 objects
% num_obstacles = 3;
% obstacles = ['annu3' ; 'annu3' ; 'annu3'];
% r0s = [1 1 1];
% rmaxs = [3 3 3];
% centers = [-4 0 ; 4 0 ; 12 0];

% %for 2 objects non-circle
% num_obstacles = 2;
% obstacles = ['roid3' ; 'roid3'];
% r0s = [0.5002 0.5002];
% rmaxs = [3 3];
% centers = [-4 0 ; 4 0];


%for 4 objects
% num_obstacles = 4;
% obstacles = ['annu2' ; 'annu2' ; 'annu2' ; 'annu2'];
% r0s = [1 1 1 1];
% rmaxs = [2 2 2 2];
% centers = [-7.5 0 ; -2.5 0 ; 2.5 0 ; 7.5 0];

%for 2 objects
num_obstacles = 2;
obstacles = ['annu2' ; 'annu2'];
r0s = [1 1];
rmaxs = [2 2];
%vertical and coming from the right
centers = [0 -2.25 ; 0 2.25];
uinc = @(x,y) exp(1j*k*-x); %incident wave

%horizontal and coming from the top
% centers = [-2.25 0 ; 2.25 0];
% uinc = @(x,y) exp(1j*k*-y); %incident wave





for i=1:num_obstacles
    r0 = r0s(i);
    rmax = rmaxs(i);
    N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
    N1s(i) = N1;
    T{i} = linspace(0,2*pi,N1);
    N2 = ceil(PPW*(rmax-r0)*k/(2*pi));
    N2s(i) = N2;
    
    fprintf(['\nGenerating grid for obstacle ' num2str(i) '\n'])
    [X,Y] = elliptic_polar(N1,N2,1.7,1e-5,10000,obstacles(i,:)); %generate grid
    
    Xgrids{i} = X;
    Ygrids{i} = Y;
    Xdisplaced{i} = X + centers(i,1);
    Ydisplaced{i} = Y + centers(i,2);
    
    U{i} = zeros(N1,N2);
    
   for l =1:L %initialize cells with interpolated functions that return zero
       f_interp{i,l} = griddedInterpolant(T{i},zeros(1,N1));
       g_interp{i,l} = griddedInterpolant(T{i},zeros(1,N1));
   end
end



for i=1:num_obstacles
    for j=1:num_obstacles
        %i,j gives from center i to obstacle j
        if i~=j
            relativeX = Xdisplaced{j} - centers(i,1);
            relativeY = Ydisplaced{j} - centers(i,2);
            [radius{i,j},angles{i,j}] = topolar(relativeX,relativeY);
        end
    end
end


parfor i=1:num_obstacles
    %get the matrix for each obstacle and the incident wave for each
    %obstacle
    A{i} = Helmholts_curvilinear_coefficient_matrix(Xgrids{i},Ygrids{i},L,k);
    incident{i} = uinc(Xdisplaced{i},Ydisplaced{i});
    
end



%figure()
ls = 0:L-1;
order = 1:num_obstacles;
for q=1:maxiter
    %clf
    %title(['iteration = ' num2str(q)])
    iteration = q
    hold on
    for i=order
        N1 = N1s(i);
        N2 = N2s(i);
        len = (N1 - 1)*(N2-1 + 2*L);
        b = zeros(len,1);
        
        usc_other_obstacles = zeros(N1-1,1); 
        
        for j=1:num_obstacles
            if i~=j
                for m=1:N1-1

                    theta = angles{j,i}(m,1);
                    r = radius{j,i}(m,1);
                    H0 = besselh(0,k*r);
                    H1 = besselh(1,k*r);

                    usc_other_obstacles(m) = usc_other_obstacles(m) ...
                        + H0*sum(arrayfun(@(l) f_interp{j,l+1}(theta) * (k*r)^(-l),ls)) ...
                        + H1*sum(arrayfun(@(l) g_interp{j,l+1}(theta) * (k*r)^(-l),ls));
                    
                end
            end
        end
        
        
        b(1:N1-1) = -incident{i}(1:N1-1,1) - usc_other_obstacles; %set b vector
        u = A{i}\b; %solve the system
        
        u_vec = u(1:(N1-1)*(N2-1));
        u_vec = reshape(u_vec,N1-1,[]);
        u_vec = [u_vec ; u_vec(1,:)];
        far = u((N1-1)*(N2-1) + 1:len);
        far = reshape(far,N1-1,2*L);
        far = [far ; far(1,1:2*L)];
        f = far(1:N1,1:2:2*L);
        g = far(1:N1,2:2:2*L);
        t = T{i};
        ls = 0:L-1;
        kR = k*rmaxs(i);
        broadcast = arrayfun(@(l) (kR)^(-l),ls);
        
        H0 = besselh(0,kR);
        H1 = besselh(1,kR);
        
        UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);
        uscat = [u_vec UN2]; 
        
        bools(i) = norm(U{i} - uscat,'fro') < tol; %check for convergence
        
        U{i} = uscat;
        
        

        for l=1:L %store farfield functions as interpolated functions
            f_interp{i,l} = griddedInterpolant(t,f(1:N1,l),'spline');
            g_interp{i,l} = griddedInterpolant(t,g(1:N1,l),'spline');
        end

        %u_total_approx = abs(uscat + uinc(Xdisplaced{i},Ydisplaced{i}));
        
        %plot the scattered wave
%         surf(Xdisplaced{i},Ydisplaced{i},abs(uscat))
        
    end
%     axis equal
%     view(2);
%     hold off
%     pause(0.1);

    if prod(bools) == 1
        break;
    end
    
%     order =  [order(2:num_obstacles) order(1)];

end

%construct final image
figure()
clf
hold on

%this next portion calculates the total wave for the whole thing. Very
%expensive because I have to use the interpolating functions on every grid
%point. THis is where most of the time is eaten up


fprintf(['\n\nConstructing total wave for all obstacles\n'])
tic;
figure()
for i=1:num_obstacles
    N1 = N1s(i);
    N2 = N2s(i);

    usc_other_obstacles = zeros(N1,N2);

    for j=1:num_obstacles
        if i~=j
            for m=1:N1
                for n=1:N2

                theta = angles{j,i}(m,n);
                r = radius{j,i}(m,n);
                H0 = besselh(0,k*r);
                H1 = besselh(1,k*r);

                usc_other_obstacles(m,n) = usc_other_obstacles(m,n) ...
                    + H0*sum(arrayfun(@(l) f_interp{j,l+1}(theta) * (k*r)^(-l),ls)) ...
                    + H1*sum(arrayfun(@(l) g_interp{j,l+1}(theta) * (k*r)^(-l),ls));

                end
            end
        end
    end
    Utot{i} = incident{i} + U{i} + usc_other_obstacles;
    surface(Xdisplaced{i},Ydisplaced{i},abs(Utot{i}))
    
end
toc
title('final construction');
view(2);
axis equal
colorbar;
shading interp;
hold off
