%testmain

PPWs = [30,35,40,45,50,55,60];
num_obstacles = [2,3];

obstacles = ['annu2';'annu2';'annu2';'annu2';'annu2';'annu2'];
r0s = [1 1 1 1 1 1];
rmaxs = [2 2 2 2 2 2];






times_jacobi = zeros(3,length(PPWs),length(num_obstacles));
iters_jacobi = zeros(length(PPWs),length(num_obstacles));
times_gs = zeros(3,length(PPWs),length(num_obstacles));
iters_gs = zeros(length(PPWs),length(num_obstacles));
% times_sor = cell(length(PPWs),length(num_obstacles));
% iters_sor = cell(length(PPWs),length(num_obstacles));


for j=1:length(num_obstacles)
    fprintf(['number of obstacles = ' num2str(num_obstacles(j)) '\n'])
    for i = 1:length(PPWs)
        
        
        PPW = PPWs(i);
        fprintf(['    PPW = ' num2str(PPW) '\n'])
        num_obs = num_obstacles(j);
        
        obs = obstacles(1:num_obs,:);
        rmax = rmaxs(1:num_obs);
        r0 = r0s(1:num_obs);
        centers = zeros(num_obs,2);
        for k=1:num_obs
            centers(k,1) = -2.5 + 5*(k-1);
        end
        
        [~,~,~,~,time_converge,time_construct,tot_time,~,~,~,~,iters] ... 
            = Jacobi_Multiple_Scattering(2*pi,PPW,-pi/2,0,8,obs,r0,rmax,centers,'spline');
        
        times_jacobi(:,i,j) = [time_converge time_construct tot_time];
        iters_jacobi(i,j) = iters;
        [~,~,~,~,time_converge,time_construct,tot_time,~,~,~,~,iters] ... 
            = Gauss_Seidel_Multiple_Scattering(2*pi,PPW,-pi/2,0,8,obs,r0,rmax,centers,'spline');
        times_gs(:,i,j) = [time_converge time_construct tot_time];
        iters_gs(i,j) = iters;
    end
end
save('convergence.mat','PPWs','num_obstacles','times_jacobi','iters_jacobi','times_gs','iters_gs')
fprintf('done\n')

