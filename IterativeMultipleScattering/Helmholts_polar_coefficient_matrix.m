function A =  Helmholts_polar_coefficient_matrix(X,Y,L,k)
[N1,N2] = size(X);
%initializa sparse matrix and the right hand side vector
numnonzero = (N1-1)*(20*L - 21 +9*N2);
size_of_A = (N1 - 1)*(N2-1 + 2*L);
A = spalloc(size_of_A,size_of_A,numnonzero);
% rhs = zeros(N1-1,N2-1 + 2*L);

%these are important constants
r = sqrt(X(:,N2-1).^2 + Y(:,N2-1).^2); %the radius of the N2-1 level
r0 = mean(sqrt(X(:,1).^2 + Y(:,1).^2)); %the outside radius
R = sqrt(X(:,N2).^2 + Y(:,N2).^2); %the outside radius (3)
R_ = mean(R);
DR = R-r;  %a vector containing delta r for the N2 level
dt = 2*pi/(N1-1); %delta theta
r_linspace = linspace(r0,R_,N2);
theta_linspace = linspace(0,2 *pi,N1);
dr = (R_-r0)/(N2-1);

%incident wave




%Fill the matrix
% tic;
% fprintf('\n\nConstructing Coefficient Matrix... \n')
%First, deal with the inner boundary conditions. Adds N1-1 nz terms
j = 1;
for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    
    A(spot,spot)=1;
end


%Interior Points. Adds (N1-1)*9*(N2-3) nz terms
for j=2:N2-2
    r =  r_linspace(j);
    alpha_plus = 1/dr^2 + 1/(2*dr * r);
    alpha_minus = 1/dr^2 - 1/(2*dr * r);
    alpha = k^2 - 2/(dr^2) - 2/(dt^2 * r^2);
    beta = 1/(dt^2 * r^2);
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        [super_diag,sub_diag] = super_sub(i,N1);

        A(spot,spot - (N1-1)) = alpha_minus;
        

        A(spot,spot + sub_diag) = beta; 
        A(spot,spot) = alpha;
        A(spot,spot + super_diag) = beta;

        
        A(spot,spot + (N1-1)) = alpha_plus;       
    end
    
    
end

%Next is the N2-1 level. This is a special case. Adds (N1-1)*(6 + 6*L) nz
%terms
j = N2-1;
r =  r_linspace(j);
alpha_plus = 1/dr^2 + 1/(2*dr * r);
alpha_minus = 1/dr^2 - 1/(2*dr * r);
alpha = k^2 - 2/(dr^2) - 2/(dt^2 * r^2);
beta = 1/(dt^2 * r^2);
kR = k*R_;
H0 = besselh(0,kR);
H1 = besselh(1,kR);

for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    base = spot + N1 - 1;
    [super_diag,sub_diag] = super_sub(i,N1);
    
    A(spot,spot - (N1-1)) = alpha_minus;
           
    A(spot,spot + sub_diag) = beta;
    A(spot,spot) = alpha;
    A(spot,spot + super_diag) = beta;        
    
    
    for l =0:L-1
        A(spot,base + 2*(N1-1)*l) = alpha_plus*H0/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l) = alpha_plus*H1/((kR)^l);
%         A(spot,base + 2*(N1 -1)*l) = alpha_plus*besselh(0,k*R)/((R)^l);
%         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = alpha_plus*besselh(1,k*R)/((R)^l);
    end
end
    
%now, for the artificial boundary. This uses the continuity of the first
%derivative. Adds (N1-1)*(1 + 6*L) nz terms
j = N2;
r =  r_linspace(j);
alpha_plus = 1/dr^2 + 1/(2*dr * r);
alpha_minus = 1/dr^2 - 1/(2*dr * r);
alpha = k^2 - 2/(dr^2) - 2/(dt^2 * r^2);
beta = 1/(dt^2 * r^2);


for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    base = spot;
    A(spot,spot -(N1-1)) = alpha_minus+alpha_plus;
    [super_diag,sub_diag] = super_sub(i,N1);
    
    for l=0:L-1
        Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
        Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
        Cl = -alpha_plus*2*dr*Al + alpha*H0/((kR)^l);
        Dl = -alpha_plus*2*dr*Bl + alpha*H1/((kR)^l);
         
        A(spot,base + super_diag + 2*(N1 -1)*l) = beta*H0/((kR)^l);
        A(spot,base + 2*(N1 -1)*l) = Cl;
        A(spot,base + sub_diag + 2*(N1 -1)*l) = beta*H0/((kR)^l);
        
        A(spot,base + super_diag + (N1-1) + 2*(N1 -1)*l) = beta*H1/((kR)^l);
        A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Dl;
        A(spot,base + sub_diag + (N1-1) + 2*(N1 -1)*l) = beta*H1/((kR)^l);
        
    end
end

%This next set of equations is for the truncated helmholts continuity. The
%commented out code is the alternate continuity of the second derivative
%condition. Truncated Helmholts adds (N1-1)*6 nz terms
j = N2 + 1;


for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
%     base = spot - (N1-1);
%     A(spot,spot -2*(N1-1)) = 2/(dr^2);
%     
%     for l=0:L-1
%         Al = k*H1/((kR)^l) + k*l*H0/((k*R)^(l+1));
%         Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
%         Ml = -2*Al/dr + k^2 * (-H0/((kR)^l) - (2*l + 1)*H1/((kR)^(l+1)) + (l+1)*l*H0/((kR)^(l+2))) - 2*H0/(dr^2 * (kR)^(l));
%         Nl = -2*Bl/dr + k^2 * ((2*l + 1)*H0/((kR)^(l+1)) + H1/((kR)^l) - (l+1)*(l+2)*H1/(kR)^(l+2)) - 2*H1/(dr^2 * (kR)^l);
%         A(spot,base + 2*(N1 -1)*l) = Ml;
%         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Nl;
%     end

    base = spot  + (N1-1) + (L-2)*2*(N1-1);
    [super_diag,sub_diag] = super_sub(i,N1);
    
    A(spot,base + super_diag) = H0/(dt^2);
    A(spot,base) = H0*(((L-1)^2)-(2/(dt^2)));
    A(spot,base + sub_diag) = H0/(dt^2);
    
    A(spot,base + super_diag + (N1-1)) = H1/(dt^2);
    A(spot,base + (N1-1)) = H1*(L^2-(2/(dt^2)));
    A(spot,base + sub_diag + (N1-1)) = H1/(dt^2);
  
  
end

%recursive equations for the karp expansion equations. 
%Adds (L-1)*((N1-1)*8) nz terms.  
for l=1:L-1
    
    j = N2 + 2 + 2*(l-1);
    
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - (N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        A(spot,spot) = 2*l;
        A(spot,base) = - (-l^2 + 2/dt^2);
        
        A(spot,base + sub_diag) = 1/dt^2;
        A(spot,base + super_diag) = 1/dt^2;
    end
    
    j = N2 + 3 + 2*(l-1);
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - 3*(N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        
        A(spot,spot) = 2*l;
        A(spot,base) = - ((l-1)^2 - 2/dt^2);
        
        A(spot,base + sub_diag) = -1/dt^2;
        A(spot,base + super_diag) = -1/dt^2;
        
    end
end


% fprintf('\nMatrix Condition Number: %d\n',condest(A));
% toc
% fprintf('\n **********************************************\n');
end

function [super_diag,sub_diag] = super_sub(i,N1)
%this helper function returns the location of the super/sub diagonal
%relative to the main diagonal. 

if i ~= 1 && i ~= N1-1
    sub_diag = -1;
    super_diag =  1;
    
elseif i == N1-1 
    sub_diag = - 1;
    super_diag = - (N1-1) + 1;

else
    sub_diag = (N1-1) - 1;
    super_diag = 1;
end    
end
