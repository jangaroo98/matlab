function uscex = ExactSolnSingleScat(r0,nterms,R,Theta,k)

eta = @(n) (1 + 1*(n>=1))*besselj(n,k*r0) *(1j)^(n) / besselh(n,1,k*r0);
uscex = R*0;
for n=0:nterms
    uscex = uscex + arrayfun(@(r,t) -eta(n) *besselh(n,1,k*r)*cos(n*t),R,Theta);
end
