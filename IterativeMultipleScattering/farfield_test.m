% LS = 5:12;
% PPWs = [30,35,40,45,50,55,60];
LS = [30];
PPWs=[40];
m = length(PPWs);
n = length(LS);
error1on2 = zeros(m,n);
error2on1 = zeros(m,n);
jj = 1;
ii = 1;

for PPW = PPWs
    
    
    for L=9:10


    k = 2*pi;
%     PPW = 40;
    phi = 0;
    z = 0;
    % L=9;
    maxiter = 200;
    tol = 1e-7;
    fprintf(['PPW = ' num2str(PPW) ', L = ' num2str(L) '\n'])

    num_obstacles = 2;
    obstacles = ['annu2' ; 'annu2'];
    r0s = [1 1];
    rmaxs = [2 2];
    centers = [0,-2.25;0, 2.25];
    uinc = @(x,y) exp(1j*k*(x*cos(phi) + y*sin(phi))); %incident wave


    Xdisplaced = cell(num_obstacles,1);
    Ydisplaced = cell(num_obstacles,1);
    Xgrids = cell(num_obstacles,1);
    Ygrids = cell(num_obstacles,1);
    U = cell(num_obstacles,1);
    A = cell(num_obstacles,1);

    parfor i=1:num_obstacles
        r0 = r0s(i);
        rmax = rmaxs(i);
        N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
        N1s(i) = N1;
        T{i} = linspace(0,2*pi,N1);
        N2 = ceil(PPW*(rmax-r0)*k/(2*pi));
        N2s(i) = N2;

        [X,Y] = elliptic_polar(N1,N2,1.7,1e-5,10000,obstacles(i,:)); %generate grid

        Xgrids{i} = X;
        Ygrids{i} = Y;
        Xdisplaced{i} = X + centers(i,1);
        Ydisplaced{i} = Y + centers(i,2);

        U{i} = zeros(N1,N2);

    end

    parfor i=1:num_obstacles
        %get the matrix for each obstacle and the incident wave for each
        %obstacle
        A{i} = Helmholts_curvilinear_coefficient_matrix(Xgrids{i},Ygrids{i},L,k,z);
        incident{i} = uinc(Xdisplaced{i},Ydisplaced{i});


        for j=1:num_obstacles
            %i,j gives from center i to obstacle j
            if i~=j
                relativeX = Xdisplaced{j} - centers(i,1);
                relativeY = Ydisplaced{j} - centers(i,2);
                [radius{i,j},angles{i,j}] = topolar(relativeX,relativeY);
            end
        end
    end

    F = cell(num_obstacles,1);
    G = cell(num_obstacles,1);


    parfor i=1:num_obstacles
        ls = 0:L-1;
        N1 = N1s(i);
        N2 = N2s(i);
        f = zeros(N1,L);
        g = zeros(N1,L);
        F{i} = f;
        G{i} = g;
    end

    for i=1:num_obstacles
        ls = 0:L-1;
        N1 = N1s(i);
        N2 = N2s(i);
        ms = 1:N1-1;
        len = (N1 - 1)*(N2-1 + 2*L);
        b = zeros(len,1);

        b(1:N1-1) = -incident{i}(1:N1-1,1); %set b vector
        u = A{i}\b; %solve the system

        u_vec = u(1:(N1-1)*(N2-1));
        u_vec = reshape(u_vec,N1-1,[]);
        u_vec = [u_vec ; u_vec(1,:)];
        far = u((N1-1)*(N2-1) + 1:len);
        far = reshape(far,N1-1,2*L);
        far = [far ; far(1,1:2*L)];
        f = far(1:N1,1:2:2*L);
        g = far(1:N1,2:2:2*L);

        F{i} = f;
        G{i} = g;


        ls = 0:L-1;
        kR = k*rmaxs(i);
        broadcast = arrayfun(@(l) (kR)^(-l),ls);

        H0 = besselh(0,kR);
        H1 = besselh(1,kR);

        UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);
        uscat = [u_vec UN2]; 


        U{i} = uscat;

    end
    figure()
    hold on
    for i=1:num_obstacles
        surface(Xdisplaced{i},Ydisplaced{i},abs(U{i} + incident{i}))
    end
    title('Approximates Inital Scattered Waves Off Obstacles')
    shading interp;
    axis equal;
    hold off


    figure()
    hold on
    for i=1:num_obstacles
        [R,Theta] = topolar(Xgrids{i},Ygrids{i});
        uscat = ExactSolnSingleScat(1,20,R,Theta,2*pi);
        surface(Xdisplaced{i},Ydisplaced{i},abs(uscat + incident{i}))
    end
    title('Actual Initial Scattered Waves Off Obstacles')
    shading interp;
    axis equal;
    hold off

    %2 from 1
    figure()
    hold on

    [R,Theta] = topolar(Xgrids{1},Ygrids{1});
    uscat = ExactSolnSingleScat(1,20,R,Theta,2*pi);
    surface(Xdisplaced{1},Ydisplaced{1},abs(uscat + incident{1}))

    [R,Theta] = topolar(Xdisplaced{2} - centers(1,1),Ydisplaced{2} - centers(1,2));
    uscat = ExactSolnSingleScat(1,100,R,Theta,2*pi);
    surface(Xdisplaced{2},Ydisplaced{2},abs(uscat + incident{2}))

    title('Actual Scattered Waves Off Obstacle 1 on Obstacle 2')
    shading interp;
    axis equal;
    hold off

    actualon2 = uscat + incident{2};


    obstaclej = zeros(N1-1,1); 

    ls = 0:L-1;
    N1 = N1s(2);
    N2 = N2s(2);
    ms = 1:N1-1;
    len = (N1 - 1)*(N2-1 + 2*L);
    b = zeros(len,1);

    usc_other_obstacles = zeros(N1,N2); 


    fj = F{1};
    gj = G{1};
    tj = T{1};
    for l=ls
        fjl = fj(1:N1,l+1);
        gjl = gj(1:N1,l+1);

    %     fjl = [fjl(end);fjl(1:end-1)];
    %     gjl = [gjl(end);gjl(1:end-1)];
    %     fjl = [fjl(2:end);fjl(1)];
    %     gjl = [gjl(2:end);gjl(1)];



        fjl_interp = griddedInterpolant(tj,fjl,'spline');
        gjl_interp = griddedInterpolant(tj,gjl,'spline');

        theta = angles{1,2};
        r = radius{1,2};
        for m=1:N1
            for n=1:N2

                v = besselh(0,k*r(m,n)) * fjl_interp(theta(m,n)) * (k*r(m))^(-l) ...
                    + besselh(1,k*r(m,n)) * gjl_interp(theta(m,n)) * (k*r(m,n))^(-l);
                usc_other_obstacles(m,n) = usc_other_obstacles(m,n) + v;
            end
        end

    end

    approxon2 = incident{2} + usc_other_obstacles;


    figure()
    hold on
    surface(Xdisplaced{1},Ydisplaced{1},abs(U{1} + incident{1}))
    surface(Xdisplaced{2},Ydisplaced{2},abs(approxon2))

    title('Approximate Scattered Waves Off Obstacle 1 on Obstacle 2')
    shading interp;
    axis equal;
    hold off

    error1on2(jj,ii) = norm(approxon2(1:N1,1) - actualon2(1:N1,1),'inf');

    fprintf(['Infinity norm obstacle 2 = ' num2str(norm(approxon2(1:N1,1) - actualon2(1:N1,1),'inf')) '\n'])


    %1 from 2
    figure()
    hold on

    [R,Theta] = topolar(Xgrids{2},Ygrids{2});
    uscat = ExactSolnSingleScat(1,20,R,Theta,2*pi);
    surface(Xdisplaced{2},Ydisplaced{2},abs(uscat + incident{2}))

    [R,Theta] = topolar(Xdisplaced{1} - centers(2,1),Ydisplaced{1} - centers(2,2));
    uscat = ExactSolnSingleScat(1,100,R,Theta,2*pi);
    surface(Xdisplaced{1},Ydisplaced{1},abs(uscat + incident{1}))

    title('Actual Scattered Wave Off Obstacle 2 on Obstacle 1')
    shading interp;
    axis equal;
    hold off

    actualon1 = uscat + incident{1};


    obstaclej = zeros(N1-1,1); 

    ls = 0:L-1;
    N1 = N1s(2);
    N2 = N2s(2);
    ms = 1:N1-1;
    len = (N1 - 1)*(N2-1 + 2*L);
    b = zeros(len,1);

    usc_other_obstacles = zeros(N1,N2); 


    fj = F{2};
    gj = G{2};
    tj = T{2};
    for l=ls
        fjl = fj(1:N1,l+1);
        gjl = gj(1:N1,l+1);

    %     fjl = [fjl(end);fjl(1:end-1)];
    %     gjl = [gjl(end);gjl(1:end-1)];
    %     fjl = [fjl(2:end);fjl(1)];
    %     gjl = [gjl(2:end);gjl(1)];



        fjl_interp = griddedInterpolant(tj,fjl,'spline');
        gjl_interp = griddedInterpolant(tj,gjl,'spline');

        theta = angles{2,1};
        r = radius{2,1};
        for m=1:N1
            for n=1:N2

                v = besselh(0,k*r(m,n)) * fjl_interp(theta(m,n)) * (k*r(m))^(-l) ...
                    + besselh(1,k*r(m,n)) * gjl_interp(theta(m,n)) * (k*r(m,n))^(-l);
                usc_other_obstacles(m,n) = usc_other_obstacles(m,n) + v;
            end
        end

    end

    approxon1 = incident{1} + usc_other_obstacles;


    figure()
    hold on
    surface(Xdisplaced{2},Ydisplaced{2},abs(U{2} + incident{2}))
    surface(Xdisplaced{1},Ydisplaced{1},abs(approxon1))

    title('Approximate Scattered Wave Off Obstacle 2 on Obstacle 1')
    shading interp;
    axis equal;
    hold off

    error2on1(jj,ii) = norm(approxon1(1:N1,1) - actualon1(1:N1,1),'inf');


    fprintf(['Infinity norm obstacle 1 = ' num2str(norm(approxon1(1:N1,1) - actualon1(1:N1,1),'inf')) '\n'])

    ii=ii+1;
    end
    jj = jj+1;
    ii = 1;
end






