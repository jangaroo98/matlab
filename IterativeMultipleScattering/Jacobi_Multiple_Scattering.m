function [Utot,Xdisplaced,Ydisplaced,ffp1,time_converge,time_construct,tot_time,ffpinferr,ffp2err,boundaryinferr,boundary2err,iters] ...
    = Jacobi_Multiple_Scattering(k,PPW,phi,z,L,obstacles,r0s,rmaxs,centers,interp_method)
tic
uinc = @(x,y) exp(1j*k*(x*cos(phi) + y*sin(phi))); %incident wave
maxiter = 200;
tol = 1e-5;
num_obstacles = length(r0s);


Xdisplaced = cell(num_obstacles,1);
Ydisplaced = cell(num_obstacles,1);
Xgrids = cell(num_obstacles,1);
Ygrids = cell(num_obstacles,1);
U = cell(num_obstacles,1);
A = cell(num_obstacles,1);



parfor i=1:num_obstacles
    r0 = r0s(i);
    rmax = rmaxs(i);
    N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
    N1s(i) = N1;
    T{i} = linspace(0,2*pi,N1);
    N2 = ceil(PPW*(rmax-r0)*k/(2*pi));
    N2s(i) = N2;
    
    [X,Y] = elliptic_polar(N1,N2,1.7,1e-5,10000,obstacles(i,:)); %generate grid
    
    Xgrids{i} = X;
    Ygrids{i} = Y;
    Xdisplaced{i} = X + centers(i,1);
    Ydisplaced{i} = Y + centers(i,2);
    
    U{i} = zeros(N1,N2);
   
end
F = cell(num_obstacles,1);
G = cell(num_obstacles,1);

parfor i=1:num_obstacles
    %get the matrix for each obstacle and the incident wave for each
    %obstacle
    A{i} = Helmholts_curvilinear_coefficient_matrix(Xgrids{i},Ygrids{i},L,k,z);
    incident{i} = uinc(Xdisplaced{i},Ydisplaced{i});
    
    
    for j=1:num_obstacles
        %i,j gives from center i to obstacle j
        if i~=j
            relativeX = Xdisplaced{j} - centers(i,1);
            relativeY = Ydisplaced{j} - centers(i,2);
            [radius{i,j},angles{i,j}] = topolar(relativeX,relativeY);
        end
    end
    N1 = N1s(i);
    f = zeros(N1,L);
    g = zeros(N1,L);
    F{i} = f;
    G{i} = g;
    
end



start = toc;


for q=1:maxiter
    F_new = cell(num_obstacles,1);
    G_new = cell(num_obstacles,1);
    
    
    err = zeros(num_obstacles,1);
    parfor i=1:num_obstacles
        ls = 0:L-1;
        N1 = N1s(i);
        N2 = N2s(i);
        ms = 1:N1-1;
        len = (N1 - 1)*(N2-1 + 2*L);
        b = zeros(len,1);
        
        usc_other_obstacles = zeros(N1-1,1); 
        
        for j=1:num_obstacles
            if i~=j
                fj = F{j};
                gj = G{j};
                tj = T{j};
%                 for m=1:N1-1
                for l=ls
                    fjl = fj(1:N1,l+1);
                    gjl = gj(1:N1,l+1);
                    
                    fjl_interp = griddedInterpolant(tj,fjl,'spline');
                    gjl_interp = griddedInterpolant(tj,gjl,'spline');
                    
                    theta = angles{j,i}(1:N1,1);
                    r = radius{j,i}(1:N1,1);
                    
                    v = arrayfun(@(m) besselh(0,k*r(m)) * fjl_interp(theta(m)) * (k*r(m))^(-l),ms) ...
                        + arrayfun(@(m) besselh(1,k*r(m)) * gjl_interp(theta(m)) * (k*r(m))^(-l),ms);
                    v = reshape(v,N1-1,1);

                    usc_other_obstacles(1:N1-1) = usc_other_obstacles(1:N1-1) + v;
                    
                    
                end
            end
        end
        
        
        b(1:N1-1) = -incident{i}(1:N1-1,1) - usc_other_obstacles; %set b vector
        u = A{i}\b; %solve the system
        
        u_vec = u(1:(N1-1)*(N2-1));
        u_vec = reshape(u_vec,N1-1,[]);
        u_vec = [u_vec ; u_vec(1,:)];
        far = u((N1-1)*(N2-1) + 1:len);
        far = reshape(far,N1-1,2*L);
        far = [far ; far(1,1:2*L)];
        f = far(1:N1,1:2:2*L);
        g = far(1:N1,2:2:2*L);
        
        F_new{i} = f;
        G_new{i} = g;
        
        
        ls = 0:L-1;
        kR = k*rmaxs(i);
        broadcast = arrayfun(@(l) (kR)^(-l),ls);
        
        H0 = besselh(0,kR);
        H1 = besselh(1,kR);
        
        UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);
        uscat = [u_vec UN2]; 
        
        err(i) = norm(U{i} - uscat,'fro'); %check for convergence
        U{i} = uscat;
        
        %plot the scattered wave
        
        
    end
    
    
    F = F_new;
    G = G_new;
    

    if sum(err) < tol
        iters = q;
        break;
    else
        iters = maxiter;
    end
end
time_converge = toc -start;
%construct final image

parfor i=1:num_obstacles
    ls = 0:L-1;
    N1 = N1s(i);
    N2 = N2s(i);
    ms = 1:N1-1;
    len = (N1 - 1)*(N2-1 + 2*L);
    b = zeros(len,1);

    usc_other_obstacles = zeros(N1,N2); 

    for j=1:num_obstacles
        if i~=j
            fj = F{j};
            gj = G{j};
            tj = T{j};
            for l=ls
                fjl = fj(1:N1,l+1);
                gjl = gj(1:N1,l+1);

                fjl_interp = griddedInterpolant(tj,fjl,'spline');
                gjl_interp = griddedInterpolant(tj,gjl,'spline');

                theta = angles{j,i};
                r = radius{j,i};
                for m=1:N1
                    for n=1:N2

                        v = besselh(0,k*r(m,n)) * fjl_interp(theta(m,n)) * (k*r(m))^(-l) ...
                            + besselh(1,k*r(m,n)) * gjl_interp(theta(m,n)) * (k*r(m,n))^(-l);
                        usc_other_obstacles(m,n) = usc_other_obstacles(m,n) + v;
                    end
                end

            end
        end
    end
    Utot{i} = incident{i} + U{i} + usc_other_obstacles;
end

time_construct = toc - start;
      
Bx = cell(num_obstacles,1);
By = cell(num_obstacles,1);
for i=1:num_obstacles
   Bx{i} = Xdisplaced{i}(1:end,end);
   By{i} = Ydisplaced{i}(1:end,end);
end


N=1000;

[u_B,far_ex] = NcylExact(2*pi,z,phi,r0s,centers,25,N,Bx,By);

far_ex = reshape(far_ex,1,N);
f0 = cell(num_obstacles,1);
g0 = cell(num_obstacles,1);

for i=1:num_obstacles
   f0{i} = griddedInterpolant(T{i},F{i}(1:end,1),interp_method);
   g0{i} = griddedInterpolant(T{i},G{i}(1:end,1),interp_method);
end
imag = 1j;

ffp=@(theta) (1-imag)/sqrt(pi) * sum(arrayfun(@(i)  (f0{i}(theta) - imag*g0{i}(theta)).*exp(-imag*k*(centers(i,1).*cos(theta)+centers(i,2).*sin(theta))),1:num_obstacles));
t = linspace(0,2*pi,N);
ffp1 = arrayfun(@(tt) ffp(tt),t);

ffpinferr = norm(ffp1 - far_ex,'inf');
ffp2err = norm(ffp1 - far_ex);

boundary2err = arrayfun(@(i) norm(u_B{i} - Utot{i}(1:end,end)),1:num_obstacles);
boundaryinferr = arrayfun(@(i) norm(u_B{i} - Utot{i}(1:end,end),'inf'),1:num_obstacles);
    
tot_time = toc;
end


