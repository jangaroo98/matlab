PPWs = [30,35];
num_obstacles = [2];
obstacles = ['annu2';'annu2';'annu2';'annu2';'annu2';'annu2'];
r0s = [1 1 1 1 1 1];
rmaxs = [2 2 2 2 2 2];
omegas = cell(length(PPWs),length(num_obstacles));
iterscell = cell(length(PPWs),length(num_obstacles));







for i =1:length(PPWs)
    for j=1:length(num_obstacles)
        
        PPW = PPWs(i);
        num_obs = num_obstacles(j);
        
        obs = obstacles(1:num_obs,:);
        rmax = rmaxs(1:num_obs);
        r0 = r0s(1:num_obs);
        centers = zeros(num_obs,2);
        for k=1:num_obs
            centers(k,1) = -2.5 + 5*(k-1);
        end
        
        f = @(omega) wsearch(omega,PPW,obs,r0,rmax,centers);
        [omegamin,fmin] = fminsearch(f,1.1);
        omegas{i,j} = omegamin;
        iterscell{i,j} = fmin;
    end
end

function iters = wsearch(omega,PPW,obs,r0,rmax,centers)
[~,~,~,~,~,~,~,~,~,~,~,iters] ... 
    = SOR_Multiple_Scattering(2*pi,PPW,-pi/2,0,8,obs,r0,rmax,centers,'spline',omega);
end