function A =  Helmholts_curvilinear_coefficient_matrix(X,Y,L,k,z)
[N1,N2] = size(X);
%initializa sparse matrix and the right hand side vector
numnonzero = (N1-1)*(20*L - 21 +9*N2);
size_of_A = (N1 - 1)*(N2-1 + 2*L);
A = spalloc(size_of_A,size_of_A,numnonzero);
% rhs = zeros(N1-1,N2-1 + 2*L);

%these are important constants
r = sqrt(X(:,N2-1).^2 + Y(:,N2-1).^2); %the radius of the N2-1 level
R = sqrt(X(:,N2).^2 + Y(:,N2).^2); %the outside radius (3)
R_ = mean(R);
DR = R-r;  %a vector containing delta r for the N2 level
dt = 2*pi/(N1-1); %delta theta


%incident wave




%Fill the matrix
% tic;
% fprintf('\n\nConstructing Coefficient Matrix... \n')
%First, deal with the inner boundary conditions. Adds N1-1 nz terms
j = 1;
for i=1:N1-1
    [super_diag,sub_diag] = super_sub(i,N1);
    xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
    xn = (-3*X(i,1) + 4*X(i,2) - X(i,3))/2;
    ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
    yn = (-3*Y(i,1) + 4*Y(i,2) - Y(i,3))/2;
    xne = (-3*X(i+super_diag,1)+4*X(i+super_diag,2)-X(i+super_diag,3)+3*X(i+sub_diag,1)-4*X(i+sub_diag,2)+X(i+sub_diag,3))/4;
    yne = (-3*Y(i+super_diag,1)+4*Y(i+super_diag,2)-Y(i+super_diag,3)+3*Y(i+sub_diag,1)-4*Y(i+sub_diag,2)+Y(i+sub_diag,3))/4;

    %Importand constants for curvillinear coordinates
    alpha = xn^2 + yn^2;
    beta = xe*xn + ye*yn;
    gamma = xe^2 + ye^2;
    J = xe*yn - xn*ye;
    ae = 2*(xn*xne + yn*yne); 
    gn = 2*(xe*xne + ye*yne); 
    %mu = ye*cos(theta) - xe*sin(theta);
    spot = (N1-1)*(j-1) + i;
    
    A(spot,spot)=1;
    
%     A(spot,spot) = -z*3*gamma + 2*(1-z)*sqrt(gamma)*J;
%     A(spot,spot + (N1-1)) = z*4*gamma;
%     A(spot,spot + 2*(N1-1)) = -z*gamma;
%     
%     A(spot,spot + sub_diag) = z*beta;
%     A(spot,spot + super_diag) = -z*beta;
%     rhs(i,1) =  -uinc_eval(i,1); % inner boundary condition
end


%Interior Points. Adds (N1-1)*9*(N2-3) nz terms
for j=2:N2-2
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        [super_diag,sub_diag] = super_sub(i,N1);
        [alpha,beta,gamma,J,ae,gn] = important_constants(i,j,X,Y,super_diag,sub_diag);

        %fill the matrix 
        A(spot,spot - (N1-1) +sub_diag) = -beta/2;
        A(spot,spot - (N1-1)) = gamma - gn/4;
        A(spot,spot - (N1-1) + super_diag) = beta/2;

        A(spot,spot+sub_diag) = alpha - ae/4; 
        A(spot,spot) = (J^2)*k^2 -2*gamma - 2*alpha;
        A(spot,spot+super_diag) = alpha + ae/4;

        A(spot,spot + (N1-1) +sub_diag) = beta/2;
        A(spot,spot + (N1-1)) = gamma + gn/4;
        A(spot,spot + (N1-1) + super_diag) = -beta/2;

    end
end

%Next is the N2-1 level. This is a special case. Adds (N1-1)*(6 + 6*L) nz
%terms
j = N2-1;


R_ = mean(sqrt(X(:,N2).^2 + Y(:,N2).^2));
kR = k*R_;
H0 = besselh(0,kR);
H1 = besselh(1,kR);

for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    base = spot + N1 - 1;
    
    [super_diag,sub_diag] = super_sub(i,N1);
    [alpha,beta,gamma,J,ae,gn] = important_constants(i,j,X,Y,super_diag,sub_diag);
    
    
    A(spot,spot - (N1-1) +sub_diag) = -beta/2;
    A(spot,spot - (N1-1)) = gamma - gn/4;
    A(spot,spot - (N1-1) + super_diag) = beta/2;

    A(spot,spot+sub_diag) = alpha - ae/4; 
    A(spot,spot) = (J^2)*k^2 -2*gamma - 2*alpha;
    A(spot,spot+super_diag) = alpha + ae/4;    
    
    
    for l =0:L-1
        A(spot,base + 2*(N1-1)*l + sub_diag) = (beta/2)*H0/((kR)^l);
        A(spot,base + 2*(N1-1)*l) = (gamma + gn/4)*H0/((kR)^l);
        A(spot,base + 2*(N1-1)*l + super_diag) = (-beta/2)*H0/((kR)^l);
        
        
        A(spot,base + N1-1 + 2*(N1 -1)*l + sub_diag) = (beta/(2))*H1/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l) = (gamma + gn/4)*H1/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l + super_diag) = (-beta/2)*H1/((kR)^l);
    end
end
    
%now, for the artificial boundary. This uses the continuity of the first
%derivative. Adds (N1-1)*(1 + 6*L) nz terms
j = N2;


for i=1:N1-1
    [super_diag,sub_diag] = super_sub(i,N1);
    
    %we need to use second order one sided difference to get the
    %derivatives
    xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
    xn = (3*X(i,N2) - 4*X(i,N2-1) + X(i,N2-2))/2;
    ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
    yn = (3*Y(i,N2) - 4*Y(i,N2-1) + Y(i,N2-2))/2;
    xne = (3*X(i+super_diag,N2)-4*X(i+super_diag,N2-1)+X(i+super_diag,N2-2)-3*X(i+sub_diag,N2)+4*X(i+sub_diag,N2-1)-X(i+sub_diag,N2-2))/4;
    yne = (3*Y(i+super_diag,N2)-4*Y(i+super_diag,N2-1)+Y(i+super_diag,N2-2)-3*Y(i+sub_diag,N2)+4*Y(i+sub_diag,N2-1)-Y(i+sub_diag,N2-2))/4;

    %Importand constants for curvillinear coordinates
    alpha = xn^2 + yn^2;
    beta = xe*xn + ye*yn;
    gamma = xe^2 + ye^2;
    J = xe*yn - xn*ye;
    ae = 2*(xn*xne + yn*yne); 
    gn = 2*(xe*xne + ye*yne); 
    
    dr = DR(i);
%     dr = mean(DR);
    spot = (N1-1)*(j-1) + i;
    base = spot;
    
    a_ = (J^2)*k^2 -2*(gamma) - 2*alpha;
    b_ = alpha - ae/(4);
    c_ = alpha + ae/(4);
    d_ = gamma - gn/(4);
    e_ = gamma + gn/(4);
    f_ = 2*beta/(4);
    
    %fill the matrix
    A(spot,spot -(N1-1)) = d_+e_;
    for l=0:L-1
        Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
        Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
         
        A(spot,base + super_diag + 2*(N1 -1)*l) = c_*H0/kR^l + 2*f_*dr*Al;
        A(spot,base + 2*(N1 -1)*l) = a_*H0/kR^l - 2*e_*dr*Al;
        A(spot,base + sub_diag + 2*(N1 -1)*l) = b_*H0/kR^l - 2*f_*dr*Al;
        
        A(spot,base + super_diag + (N1-1) + 2*(N1 -1)*l) = c_*H1/kR^l + 2*f_*dr*Bl;
        A(spot,base + N1 - 1 + 2*(N1 -1)*l) =a_*H1/kR^l - 2*e_*dr*Bl;
        A(spot,base + sub_diag + (N1-1) + 2*(N1 -1)*l) = b_*H1/kR^l - 2*f_*dr*Bl;
        
    end
end

%This next set of equations is for the truncated helmholts continuity. The
%commented out code is the alternate continuity of the second derivative
%condition. Truncated Helmholts adds (N1-1)*6 nz terms
j = N2 + 1;


for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    base = spot - (N1-1);
    A(spot,spot -2*(N1-1)) = 2/(dr^2);
    
    for l=0:L-1
        Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
        Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
        Ml = -2*Al/dr + k^2 * (-H0/((kR)^l) - (2*l + 1)*H1/((kR)^(l+1)) + (l+1)*l*H0/((kR)^(l+2))) - 2*H0/(dr^2 * (kR)^(l));
        Nl = -2*Bl/dr + k^2 * ((2*l + 1)*H0/((kR)^(l+1)) + H1/((kR)^l) - (l+1)*(l+2)*H1/(kR)^(l+2)) - 2*H1/(dr^2 * (kR)^l);
        A(spot,base + 2*(N1 -1)*l) = Ml;
        A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Nl;
    end
%     spot = (N1-1)*(j-1) + i;
%     base = spot  + (N1-1) + (L-2)*2*(N1-1);
%     [super_diag,sub_diag] = super_sub(i,N1);
%     
%     A(spot,base + super_diag) = H0/(dt^2);
%     A(spot,base) = H0*(((L-1)^2)-(2/(dt^2)));
%     A(spot,base + sub_diag) = H0/(dt^2);
%     
%     A(spot,base + super_diag + (N1-1)) = H1/(dt^2);
%     A(spot,base + (N1-1)) = H1*(L^2-(2/(dt^2)));
%     A(spot,base + sub_diag + (N1-1)) = H1/(dt^2);
  
  
end

%recursive equations for the karp expansion equations. 
%Adds (L-1)*((N1-1)*8) nz terms.  
for l=1:L-1
    
    j = N2 + 2 + 2*(l-1);
    
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - (N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        A(spot,spot) = 2*l;
        A(spot,base) = - (-l^2 + 2/dt^2);
        
        A(spot,base + sub_diag) = 1/dt^2;
        A(spot,base + super_diag) = 1/dt^2;
    end
    
    j = N2 + 3 + 2*(l-1);
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - 3*(N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        
        A(spot,spot) = 2*l;
        A(spot,base) = - ((l-1)^2 - 2/dt^2);
        
        A(spot,base + sub_diag) = -1/dt^2;
        A(spot,base + super_diag) = -1/dt^2;
        
    end
end


% fprintf('\nMatrix Condition Number: %d\n',condest(A));
% toc
% fprintf('\n **********************************************\n');
end

function [super_diag,sub_diag] = super_sub(i,N1)
%this helper function returns the location of the super/sub diagonal
%relative to the main diagonal. 

if i ~= 1 && i ~= N1-1
    sub_diag = -1;
    super_diag =  1;
    
elseif i == N1-1 
    sub_diag = - 1;
    super_diag = - (N1-1) + 1;

else
    sub_diag = (N1-1) - 1;
    super_diag = 1;
end    
end


function [alpha,beta,gamma,J,ae,gn]=important_constants(i,j,X,Y,super_diag,sub_diag) 
%take derivatives and partial derivatives
xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
xn = 0.5*(X(i,j+1) - X(i,j-1));
ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
yn = 0.5*(Y(i,j+1) - Y(i,j-1));
xne = 0.25*(X(i+sub_diag,j-1)+X(i+super_diag,j+1)-X(i+super_diag,j-1)-X(i+sub_diag,j+1));
yne = 0.25*(Y(i+sub_diag,j-1)+Y(i+super_diag,j+1)-Y(i+super_diag,j-1)-Y(i+sub_diag,j+1));

%Importand constants for curvillinear coordinates
alpha = xn^2 + yn^2;
beta = xe*xn + ye*yn;
gamma = xe^2 + ye^2;
J = xe*yn - xn*ye;
ae = 2*(xn*xne + yn*yne); 
gn = 2*(xe*xne + ye*yne); 
end