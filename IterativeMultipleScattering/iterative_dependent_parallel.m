%Iterative_main_copy


%define a lot of importand constants, as well as set the obstacle and
%centers data. 
k = 2*pi;
PPW = 30;
phi = -pi/2;
z = 0;
L=10;
maxiter = 200;
tol = 1e-5;

%for 3 objects
num_obstacles = 3;
obstacles = ['annu2' ; 'annu2' ; 'annu2'];
r0s = [1 1 1];
rmaxs = [2 2 2];
centers = [-2.5 0 ; 2.5 0 ; 7.5 0];

% %for 4 objects
% num_obstacles = 7;
% obstacles = ['annu2' ; 'annu2' ; 'annu2' ; 'annu2' ; 'annu2' ; 'annu2' ; 'annu2'];
% r0s = [1 1 1 1 1 1 1];
% rmaxs = [2 2 2 2 2 2 2];
% centers = [-7.5 0 ; -2.5 0 ; 2.5 0 ; 7.5 0 ; -7.5 -5 ; -2.5 5 ; 2.5 -5 ; 7.5 5];

% %for 2 objects non-circle
% num_obstacles = 3;
% obstacles = ['roid3'; 'leaf3' ; 'roid3'];
% r0s = [0.5002 0.5002 0.5002];
% rmaxs = [3 3 3];
% centers = [-5.1 0 ; 0 3.2 ; 5.1 0];

% % for 2 objects
% num_obstacles = 2;
% obstacles = ['annu2' ; 'annu2'];
% r0s = [1 1];
% rmaxs = [2 2];
% centers = [-2.25 0 ; 2.25 0];
% % centers = [0,-2.25 ;0, 2.25];
% % centers=[-2.25 -1; 2.25 1];



uinc = @(x,y) exp(1j*k*(x*cos(phi) + y*sin(phi))); %incident wave


Xdisplaced = cell(num_obstacles,1);
Ydisplaced = cell(num_obstacles,1);
Xgrids = cell(num_obstacles,1);
Ygrids = cell(num_obstacles,1);
U = cell(num_obstacles,1);
A = cell(num_obstacles,1);



parfor i=1:num_obstacles
    r0 = r0s(i);
    rmax = rmaxs(i);
    N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
    N1s(i) = N1;
    T{i} = linspace(0,2*pi,N1);
    N2 = ceil(PPW*(rmax-r0)*k/(2*pi));
    N2s(i) = N2;
    
    fprintf(['\nGenerating grid for obstacle ' num2str(i) '\n'])
    [X,Y] = elliptic_polar(N1,N2,1.7,1e-5,10000,obstacles(i,:)); %generate grid
    
    Xgrids{i} = X;
    Ygrids{i} = Y;
    Xdisplaced{i} = X + centers(i,1);
    Ydisplaced{i} = Y + centers(i,2);
    
    U{i} = zeros(N1,N2);
   
end




parfor i=1:num_obstacles
    %get the matrix for each obstacle and the incident wave for each
    %obstacle
    A{i} = Helmholts_curvilinear_coefficient_matrix(Xgrids{i},Ygrids{i},L,k,z);
    incident{i} = uinc(Xdisplaced{i},Ydisplaced{i});
    
    
    for j=1:num_obstacles
        %i,j gives from center i to obstacle j
        if i~=j
            relativeX = Xdisplaced{j} - centers(i,1);
            relativeY = Ydisplaced{j} - centers(i,2);
            [radius{i,j},angles{i,j}] = topolar(relativeX,relativeY);
        end
    end
end

F = cell(num_obstacles,1);
G = cell(num_obstacles,1);

figure()
parfor i=1:num_obstacles
    ls = 0:L-1;
    N1 = N1s(i);
    N2 = N2s(i);
    f = zeros(N1,L);
    g = zeros(N1,L);
    F{i} = f;
    G{i} = g;
end

order = 1:num_obstacles;
for q=1:maxiter
    iteration = q;
    clf
    title(['plot of scattered wave, iteration = ' num2str(q)])
    F_new = cell(num_obstacles,1);
    G_new = cell(num_obstacles,1);
    hold on
    err = 0;
    for i=1:num_obstacles
        ls = 0:L-1;
        N1 = N1s(i);
        N2 = N2s(i);
        ms = 1:N1-1;
        len = (N1 - 1)*(N2-1 + 2*L);
        b = zeros(len,1);
        
        
        vs = cell(num_obstacles);
        parfor j=1:num_obstacles
            if i~=j
                fj = F{j};
                gj = G{j};
                tj = T{j};
                obstaclej = zeros(N1-1,1); 
%                 for m=1:N1-1
                for l=ls
                    fjl = fj(1:N1,l+1);
                    gjl = gj(1:N1,l+1);
                    
                    fjl_interp = griddedInterpolant(tj,fjl,'spline');
                    gjl_interp = griddedInterpolant(tj,gjl,'spline');
                    
                    theta = angles{j,i}(1:N1,1);
                    r = radius{j,i}(1:N1,1);
                    
                    v = arrayfun(@(m) besselh(0,k*r(m)) * fjl_interp(theta(m)) * (k*r(m))^(-l),ms) ...
                        + arrayfun(@(m) besselh(1,k*r(m)) * gjl_interp(theta(m)) * (k*r(m))^(-l),ms);
                    v = reshape(v,N1-1,1);

                    obstaclej = obstaclej + v;
                    %vs{j} = v;
                    
                end
                vs{j} = obstaclej;
            end
        end
        usc_other_obstacles = zeros(N1-1,1);
        for j=1:num_obstacles
            if j~=i
                usc_other_obstacles = usc_other_obstacles + vs{j};
            end
        end
        b(1:N1-1) = -incident{i}(1:N1-1,1) - usc_other_obstacles; %set b vector
        u = A{i}\b; %solve the system
        
        u_vec = u(1:(N1-1)*(N2-1));
        u_vec = reshape(u_vec,N1-1,[]);
        u_vec = [u_vec ; u_vec(1,:)];
        far = u((N1-1)*(N2-1) + 1:len);
        far = reshape(far,N1-1,2*L);
        far = [far ; far(1,1:2*L)];
        f = far(1:N1,1:2:2*L);
        g = far(1:N1,2:2:2*L);
        
        F{i} = f;
        G{i} = g;
        
        
        ls = 0:L-1;
        kR = k*rmaxs(i);
        broadcast = arrayfun(@(l) (kR)^(-l),ls);
        
        H0 = besselh(0,kR);
        H1 = besselh(1,kR);
        
        UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);
        uscat = [u_vec UN2]; 
        
        err = err + norm(U{i} - uscat,'fro'); %get total error
        U{i} = uscat;
        
        %plot the scattered wave
        
        
    end
    hold on
    for i=1:num_obstacles
        surf(Xdisplaced{i},Ydisplaced{i},abs(U{i}))
    end
    axis equal
    view(2);
    hold off
    pause(0.1);
    
    
    
    if err < tol
        break;
    end
    


end

%construct final image
figure()
clf
hold on

%this next portion calculates the total wave for the whole thing. Very
%expensive because I have to use the interpolating functions on every grid
%point. THis is where most of the time is eaten up


fprintf(['\n\nConstructing total wave for all obstacles\n'])
tic;

parfor i=1:num_obstacles
    ls = 0:L-1;
    N1 = N1s(i);
    N2 = N2s(i);
    ms = 1:N1-1;
    len = (N1 - 1)*(N2-1 + 2*L);
    b = zeros(len,1);

    usc_other_obstacles = zeros(N1,N2); 

    for j=1:num_obstacles
        if i~=j
            fj = F{j};
            gj = G{j};
            tj = T{j};
            for l=ls
                fjl = fj(1:N1,l+1);
                gjl = gj(1:N1,l+1);

                fjl_interp = griddedInterpolant(tj,fjl,'spline');
                gjl_interp = griddedInterpolant(tj,gjl,'spline');

                theta = angles{j,i};
                r = radius{j,i};
                for m=1:N1
                    for n=1:N2

                        v = besselh(0,k*r(m,n)) * fjl_interp(theta(m,n)) * (k*r(m))^(-l) ...
                            + besselh(1,k*r(m,n)) * gjl_interp(theta(m,n)) * (k*r(m,n))^(-l);
                        usc_other_obstacles(m,n) = usc_other_obstacles(m,n) + v;
                    end
                end

            end
        end
    end
    Utot{i} = incident{i} + U{i} + usc_other_obstacles;
end
     
figure()
hold on
for i=1:num_obstacles
    surface(Xdisplaced{i},Ydisplaced{i},abs(Utot{i}))
end
    
toc
title('Plot of Converged Total Wave');
view(2);
axis equal
colorbar;
shading interp;
hold off


Bx = cell(num_obstacles,1);
By = cell(num_obstacles,1);
for i=1:num_obstacles
   Bx{i} = Xdisplaced{i}(1:end,end);
   By{i} = Ydisplaced{i}(1:end,end);
end


N=1000;

[u_B,far_ex] = NcylExact(k,0,phi,r0s,centers,100,N,Bx,By);

far_ex = reshape(far_ex,1,N);
f0 = cell(num_obstacles,1);
g0 = cell(num_obstacles,1);

for i=1:num_obstacles
   f0{i} = griddedInterpolant(T{i},F{i}(1:end,1),'spline');
   g0{i} = griddedInterpolant(T{i},G{i}(1:end,1),'spline');
end
imag = 1j;

ffp=@(theta) (1-imag)/sqrt(pi) * sum(arrayfun(@(i)  (f0{i}(theta) - imag*g0{i}(theta)).*exp(-imag*k*(centers(i,1).*cos(theta)+centers(i,2).*sin(theta))),1:num_obstacles));
t = linspace(0,2*pi,N);
ffp1 = arrayfun(@(tt) ffp(tt),t);

ffpinferr = norm(abs(ffp1) - abs(far_ex),'inf');
ffp2err = norm(abs(ffp1) - abs(far_ex));

boundary2err = arrayfun(@(i) norm(abs(u_B{i}) - abs(U{i}(1:end,end))),1:num_obstacles);
boundaryinferr = arrayfun(@(i) norm(abs(u_B{i}) - abs(U{i}(1:end,end)),'inf'),1:num_obstacles);

figure()
hold on
for i=1:num_obstacles
    plot3(Bx{i},By{i},abs(u_B{i}),'b','DisplayName','True Boundary')
    plot3(Bx{i},By{i},abs(U{i}(1:end,end)),'r*','DisplayName','Approximated Boundary')
end
legend()
hold off

figure()
polarplot(t,abs(far_ex))
hold on 
polarplot(t,abs(ffp1),'--')

legend('True Solution','Numerical Approximation')
hold off

fprintf(['Boundary LIninity = ' num2str(boundaryinferr) '\n'])
fprintf(['Farfield LIninity = ' num2str(ffpinferr) '\n'])

    


