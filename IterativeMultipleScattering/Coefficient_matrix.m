function [U,Utot,FarFs,A,x,y] = Coefficient_matrix(PPW,RINF,R0,K,theta,Z,NT)

imag = 1i;
N1 = ceil(PPW*(2*pi*R0)*K/(2*pi));            % Number of Xi-curves
N2 = ceil(PPW*(RINF-R0)*K/(2*pi));            % Number of Eta-curves
tol = 1e-6;         % Tolerance grid 
N = 2000;           % Max number of SOR iterations
w = 1.80;           % SOR parameter

NTAE = NT + 1;

x = zeros(N1,N2);           % X-coordinates 
y = zeros(N1,N2);           % Y-coordinates 
           % Field 
     % Exact Far-Field Pattern
     % Auxilinary Far-field functions F0, F1, F2, ...

uinc = zeros(N1,N2);        % Prescribed Values for Incident Field
anginc = 2*pi/(N1-1);       % Angular Increment

[x,y] = Circle(N1,R0,1,anginc,x,y);
[x,y] = Circle(N1,RINF,N2,anginc,x,y);

[x,y] = InitialGrid(N1,N2,x,y);

[x,y] = gridgen(N1,N2,tol,N,w,x,y);


[jacobian,alpha,beta,gamma,axi,geta,lambda,kappa,mu] = ...
    Metrics(N1,N2,theta,x,y);

for i=1:N1
    for j=1:N2
        uinc(i,j) = exp(imag*K*(x(i,j)*cos(theta)+y(i,j)*sin(theta)));
    end
end


[U, FarFs,A] = Helm_Solver(NTAE,K,Z,N1,N2,anginc,RINF,jacobian,alpha,beta,gamma,axi,geta,lambda,kappa,mu,uinc);
Utot = U + uinc;

end

function [x,y] = Circle(N1,R,j,anginc,x,y)


ang = 2*pi;
for i=1:N1
    x(i,j) = R*cos(ang);
    y(i,j) = R*sin(ang);
    ang = ang - anginc;
end
end

function [x,y] =  InitialGrid(N1,N2,x,y)

    for i=1:N1
        for j=2:N2-1
            x(i,j) = (j-1)/(N2-1)*(x(i,N2)-x(i,1)) + x(i,1);
            y(i,j) = (j-1)/(N2-1)*(y(i,N2)-y(i,1)) + y(i,1);
        end
    end

end

function [x,y] = gridgen(N1,N2,tol,N,w,x,y)



% Main loop
k = 0;          % iteration counter
error2 = 0;     % Error in vertor norm 2
err = tol;     % absolute error

    while(k<N && err >= tol && error2 < 10)

        k = k+1;

        % Making a copy of the current coordinate points
        xc = x;
        yc = y;

        % Performing grid generations
        for i=1:N1-1
            d = i-1;
            e = i+1;
            if (i==1), d=N1-1; end

            for j=2:N2-1
                f = j-1;
                gg = j+1;

                xxi = 0.5*(xc(e,j)-x(d,j));
                yxi = 0.5*(yc(e,j)-y(d,j));
                xeta = 0.5*(xc(i,gg)-x(i,f));
                yeta = 0.5*(yc(i,gg)-y(i,f));
                a = xeta^2+yeta^2;
                b = xxi*xeta+yxi*yeta;
                g = xxi^2+yxi^2;

                aux1 = 0.5/(a+g);
                aux3 = xc(e,gg)-xc(e,f)-x(d,gg)+x(d,f);
                aux6 = yc(e,gg)-yc(e,f)-y(d,gg)+y(d,f);

                % Approximate coordinate x
                aux2 = a*(xc(e,j)+x(d,j));
                aux4 = g*(xc(i,gg)+x(i,f));
                aux10 = ((xxi*aux3+yxi*aux6)*xeta+(xeta*aux3+yeta*aux6)*xxi)/4;
                x(i,j) = aux1*(aux2-.5*b*aux3+aux4+aux10);

                % Approximate coordinate y
                aux5 = a*(yc(e,j)+y(d,j));
                aux7 = g*(yc(i,gg)+y(i,f));
                aux10 = ((xxi*aux3+yxi*aux6)*yeta+(xeta*aux3+yeta*aux6)*yxi)/4;
                y(i,j) = aux1*(aux5-.5*b*aux6+aux7+aux10);

                % SOR : accelerating convergence
                x(i,j) = (1.-w)*xc(i,j)+w*x(i,j);
                y(i,j) = (1.-w)*yc(i,j)+w*y(i,j);
            end
        end

        % Periodicity Condition
        x(N1,:) = x(1,:);
        y(N1,:) = y(1,:);

        % ------------------------------------------------
        % Convergence Check
        err = max(max(max(abs(x-xc))),max(max(abs(y-yc))));


    end
end

function [jacobian,alpha,beta,gamma,axi,geta,lambda,kappa,mu] = Metrics(N1,N2,theta,x,y)

    jacobian = zeros(N1,N2);    % jacobian 
    alpha = zeros(N1,N2);       % alpha metric factor 
    gamma = zeros(N1,N2);       % gamma metric factor 
    beta = zeros(N1,N2);        % beta metric factor 
    axi = zeros(N1,N2);         % metric factor derivatives
    geta = zeros(N1,N2);        % metric factor derivatives
    lambda = zeros(N1,1);       % Auxiliary variables
    kappa = zeros(N1,1);        % Auxiliary variables
    mu = zeros(N1,1);           % Auxiliary variables



    % --------------------------------------------------
    % Inner Boundary Points
    for i=1:N1
        d=i-1;
        e=i+1;
        if (i==1), d=N1-1; end 
        if (i==N1), e=2; end

        xxi = (x(e,1)-x(d,1))/2;                      
        xeta = (-3*x(i,1)+4*x(i,2)-x(i,3))/2;     
        yxi = (y(e,1)-y(d,1))/2;                      
        yeta =(-3*y(i,1)+4*y(i,2)-y(i,3))/2;
        xxieta = (-3*x(e,1)+4*x(e,2)-x(e,3)+3*x(d,1)-4*x(d,2)+x(d,3))/4;  
        yxieta = (-3*y(e,1)+4*y(e,2)-y(e,3)+3*y(d,1)-4*y(d,2)+y(d,3))/4;  

        jacobian(i,1) = xxi*yeta-xeta*yxi;        
        alpha(i,1) = xeta^2+yeta^2;                          
        gamma(i,1) = xxi^2+yxi^2;                            
        beta(i,1) = xxi*xeta+yxi*yeta; 
        axi(i,1) = xeta*xxieta+yeta*yxieta;
        geta(i,1) = xxi*xxieta+yxi*yxieta;
        mu(i) = yxi*cos(theta)-xxi*sin(theta);

    end

    % --------------------------------------------------
    % Interior Points
    for j=2:N2-1
        f=j-1;
        g=j+1;

        for i=1:N1
            d=i-1;
            e=i+1;
            if (i==1), d=N1-1; end
            if (i==N1), e=2; end

            xxi=(x(e,j)-x(d,j))/2;       
            xeta=(x(i,g)-x(i,f))/2;     
            yxi=(y(e,j)-y(d,j))/2;       
            yeta=(y(i,g)-y(i,f))/2;
            xxieta = (x(e,g)-x(e,f)-x(d,g)+x(d,f))/4;
            yxieta = (y(e,g)-y(e,f)-y(d,g)+y(d,f))/4;

            jacobian(i,j) = xxi*yeta-xeta*yxi; 
            alpha(i,j) = xeta^2+yeta^2;     
            gamma(i,j) = xxi^2+yxi^2;       
            beta(i,j) = xxi*xeta+yxi*yeta;
            axi(i,j) = xeta*xxieta+yeta*yxieta;
            geta(i,j) = xxi*xxieta+yxi*yxieta;

        end
    end

    % --------------------------------------------------
    % Outer Boundary Points
    for i=1:N1
        d=i-1;
        e=i+1;
        if (i==1), d=N1-1; end 
        if (i==N1), e=2; end 

        xxi = (x(e,N2)-x(d,N2))/2;                      
        xeta = (3*x(i,N2)-4*x(i,N2-1)+x(i,N2-2))/2;     
        yxi = (y(e,N2)-y(d,N2))/2;
        yeta = (3*y(i,N2)-4*y(i,N2-1)+y(i,N2-2))/2;
        xxieta = (3*x(e,N2)-4*x(e,N2-1)+x(e,N2-2)-3*x(d,N2)+4*x(d,N2-1)-x(d,N2-2))/4;  
        yxieta = (3*y(e,N2)-4*y(e,N2-1)+y(e,N2-2)-3*y(d,N2)+4*y(d,N2-1)-y(d,N2-2))/4; 

        jacobian(i,N2) = xxi*yeta-xeta*yxi;        
        alpha(i,N2) = xeta^2+yeta^2;                         
        gamma(i,N2) = xxi^2+yxi^2;                            
        beta(i,N2) = xxi*xeta+yxi*yeta;   
        axi(i,N2) = xeta*xxieta+yeta*yxieta;
        geta(i,N2) = xxi*xxieta+yxi*yxieta;

        lambda(i) = x(i,N2)*yeta-y(i,N2)*xeta;
        kappa(i) = y(i,N2)*xxi-x(i,N2)*yxi;
    end

end

function [u, FarFs, A, b] = Helm_Solver(NTAE,K,Z,N1,N2,anginc,R,jacobian,alpha,beta,gamma,axi,geta,lambda,kappa,mu,uinc)
u = zeros(N1,N2);
FarFs = zeros(N1,NTAE);
% Auxiliary Variables
A = sparse((N2+1+NTAE)*(N1-1),(N2+1+NTAE)*(N1-1));    % Main Coefficient Marix
b = zeros((N2+1+NTAE)*(N1-1),1);                 % Right Handside Vector


% --------------------------------------------------------------------
% Building Matrix A
% --------------------------------------------------------------------
fprintf('\n Constructing Matrix ... \n ');
pause(0.01);
tic
% Inner Boundary Points (at Obstacle) 
for i=1:N1-1
   % Entry
    n = i;
    A(n,n) = -Z*3*gamma(i,1) + 2*(1-Z)*sqrt(gamma(i,1))*jacobian(i,1);
    A(n,n+(N1-1)) = Z*4*gamma(i,1);
    A(n,n+2*(N1-1)) = -Z*gamma(i,1);  
    
    %Entry
    m = n+1;
    if (i+1==N1), m = n-(N1-2); end
    A(n,m) = -Z*beta(i,1);
    %Entry
    m = n-1;
    if (i-1==0), m = n+N1-2; end
    A(n,m) = Z*beta(i,1);  
end

% Interior Points
for i=1:N1-1
    for j=2:N2
        n = (j-1)*(N1-1) + i;
        
        % Entry
        A(n,n) = (K*jacobian(i,j))^2 - 2*(alpha(i,j)+gamma(i,j));
        A(n,n-(N1-1)) = gamma(i,j) - geta(i,j)/2;
        A(n,n+(N1-1)) = gamma(i,j) + geta(i,j)/2;

        % Entry
        m = n+1;
        if (i+1==N1), m = n-(N1-2); end
        A(n,m) = alpha(i,j) + axi(i,j)/2;
        % Entry
        m = n-1;
        if (i-1==0), m = n+N1-2; end
        A(n,m) = alpha(i,j) - axi(i,j)/2;
        
        % Entry
        m = n-(N1-1)+1;
        if (i+1==N1), m = n-(N1-1)-(N1-2); end
        A(n,m) = 0.5*beta(i,j);
        % Entry
        m = n-(N1-1)-1;
        if (i-1==0), m = n-(N1-1)+(N1-2); end
        A(n,m) = -0.5*beta(i,j);
        
        % Entry
        m = n+(N1-1)+1;
        if (i+1==N1), m = n+(N1-1)-(N1-2); end
        A(n,m) = -0.5*beta(i,j);
        % Entry
        m = n+(N1-1)-1;
        if (i-1==0), m = n+(N1-1)+(N1-2); end
        A(n,m) = 0.5*beta(i,j);
    end
end




% Ghost Points
for i=1:N1-1
    n = N2*(N1-1)+i;
    
    % Entry
    A(n,n) = kappa(i)/2;
    A(n,n-2*(N1-1)) = -kappa(i)/2;
    A(n,n-(N1-1)) = (- 1i*K + 1/(2*R) )*jacobian(i,N2)*R;

    % Entry
    m = n-(N1-1)+1;
    if (i==N1-1), m = n-(N1-1)-(N1-2); end
    A(n,m) = lambda(i)/2;
    
    % Entry
    m = n -(N1-1)-1;
    if (i==1), m = n-(N1-1)+(N1-2); end
    A(n,m) = - lambda(i)/2;
     
    % Entry
    for l=1:NTAE
        m = n + l*(N1-1);
        A(n,m) = jacobian(i,N2)*R*K*exp(1i*K*R)*(l-1)/(K*R)^((l-1)+3/2);
    end
end   


% Far-field Function F0
for i=1:N1-1
    n = (N2+1)*(N1-1)+i;

    A(n,n-2*(N1-1)) = -1;
    A(n,n) = exp(1i*K*R)/(K*R)^(1/2);
    
    for l=2:NTAE
        m = n + (l-1)*(N1-1);
        A(n,m) = exp(1i*K*R)/(K*R)^((l-1) + 1/2);
    end
end

% Far-field Functions F1, F2, ...
for l=2:NTAE
    for i=1:N1-1
        n = (N2+l)*(N1-1)+i;

        A(n,n) = -2i*(l-1);
        A(n,n-(N1-1)) = ((l-1)-1/2)^2 - 2/(anginc^2);

        % Entry
        m = n-(N1-1) + 1;
        if (i==N1-1), m = n-(N1-1)-(N1-2); end
        A(n,m) = 1/(anginc^2);
        
        % Entry
        m = n-(N1-1) - 1;
        if (i==1), m = n-(N1-1)+(N1-2); end
        A(n,m) = 1/(anginc^2);
    
    end
end


toc

% --------------------------------------------------------------------
% Building Vector b
% --------------------------------------------------------------------
for i=1:N1-1
    b(i) = 2*(1i*K*Z*mu(i)-(1-Z)*sqrt(gamma(i)))*jacobian(i,1)*uinc(i,1);
end

% --------------------------------------------------------------------
% Solving System
% --------------------------------------------------------------------          
fprintf('\n Solving System ... \n ');
pause(0.01);
tic  
sol = A\b;
toc

for j=1:N2
    for i=1:N1-1
        n = (j-1)*(N1-1) + i;
        u(i,j) = sol(n);
    end
end

for l=1:NTAE
    for i=1:N1-1
        n = (N2+l)*(N1-1) + i;
        FarFs(i,l) = sol(n);        
    end
end


 
% --------------------------------------------------------------------
% Periodicity Condition
% --------------------------------------------------------------------
u(N1,:) = u(1,:);
FarFs(N1,:) = FarFs(1,:);


end





