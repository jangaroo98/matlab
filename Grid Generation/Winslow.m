function iterations = Winslow(N1,N2,w,tol,maxiter,region)
%really good w = 1.88 when n1,n2 = 50, from experimenting on the swan domain
%w = 1.902 if n1,n2 = 61 on swan
[BX,BY,simplyconnected] = boundary_of_region(N1,N2,region);
if simplyconnected
    [IX,IY] = interior_grid(N1,N2);
    BX(2:N1-1,2:N2-1) = IX;
    BY(2:N1-1,2:N2-1) = IY;
    u = zeros(N1,N2);
    surface(BX,BY,u)
    pause()

    x_old = BX;
    y_old = BY;

    x_new = BX;
    y_new = BY;

    for k=1:maxiter
        for i=2:N1-1
            for j=2:N2-1
                
                xe = 0.5*(x_old(i+1,j) - x_new(i-1,j));
                xn = 0.5*(x_old(i,j+1) - x_new(i,j-1));
                ye = 0.5*(y_old(i+1,j) - y_new(i-1,j));
                yn = 0.5*(y_old(i,j+1) - y_new(i,j-1));
                
                xne = 0.25*(x_new(i-1,j-1)+x_old(i+1,j+1)-x_new(i+1,j-1)-x_old(i-1,j+1));
                yne = 0.25*(y_new(i-1,j-1)+y_old(i+1,j+1)-y_new(i+1,j-1)-y_old(i-1,j+1));
                
                alphaij = xn^2 + yn^2;
                betaij = xe*xn + ye*yn;
                Hij = xe^2 + ye^2;
                ae = 2*xn*xne + 2*yn*yne;
                Hn = 2*xe*xne + 2*ye*yne;
                Z1 = 0.5*(ae*xe + Hn*xn)/(alphaij+Hij);
                Z2 = 0.5*(ae*ye + Hn*yn)/(alphaij+Hij);
                Cij = alphaij/(2*(Hij+alphaij));
                Dij = -betaij/(4*(Hij + alphaij));
                Eij = Hij/(2*(Hij + alphaij));
                
                x_hatij = Cij*(x_new(i-1,j) + x_old(i+1,j)) + Dij*(x_new(i-1,j-1)+x_old(i+1,j+1)-x_old(i+1,j-1)-x_new(i-1,j+1)) + Eij*(x_new(i,j-1)+x_old(i,j+1));
                y_hatij = Cij*(y_new(i-1,j) + y_old(i+1,j)) + Dij*(y_new(i-1,j-1)+y_old(i+1,j+1)-y_old(i+1,j-1)-y_new(i-1,j+1)) + Eij*(y_new(i,j-1)+y_old(i,j+1)); 

                x_new(i,j) = w*x_hatij + (1-w)*x_old(i,j);
                y_new(i,j) = w*y_hatij + (1-w)*y_old(i,j);

            end
        end

        diffx = x_new - x_old;
        diffy = y_new - y_old;
        bool1 = norm(diffx,Inf) < tol;
        bool2 = norm(diffy,Inf) < tol;
        if (bool1 && bool2)
            iterations = k;
            break
        end
        x_old = x_new;
        y_old = y_new;
        clf
        surface(x_new,y_new,u)
        pause(0.02)
    end
    if ~(bool1 || bool2)
        iterations = maxiter;
    end

    surface(x_new,y_new,u)

else
    [BX,BY] = initial_grid_multiply_connected(BX,BY);
    u = zeros(N1,N2);
    surface(BX,BY,u)
    pause()
    
    x_old = [BX ;BX(2,:)];
    y_old = [BY ;BY(2,:)];
    

    x_new = x_old;
    y_new = y_old;
    %now x_new is n1+2 by n2
    for k=1:maxiter
        
        
        for i=2:N1
            for j=2:N2-1

                xe = 0.5*(x_old(i+1,j) - x_new(i-1,j));
                xn = 0.5*(x_old(i,j+1) - x_new(i,j-1));
                ye = 0.5*(y_old(i+1,j) - y_new(i-1,j));
                yn = 0.5*(y_old(i,j+1) - y_new(i,j-1));
                
                xne = 0.25*(x_new(i-1,j-1)+x_old(i+1,j+1)-x_new(i+1,j-1)-x_old(i-1,j+1));
                yne = 0.25*(y_new(i-1,j-1)+y_old(i+1,j+1)-y_new(i+1,j-1)-y_old(i-1,j+1));
                
                alphaij = xn^2 + yn^2;
                betaij = xe*xn + ye*yn;
                Hij = xe^2 + ye^2;
                ae = 2*xn*xne + 2*yn*yne;
                Hn = 2*xe*xne + 2*ye*yne;
                Z1 = 0.5*(ae*xe + Hn*xn)/(alphaij+Hij);
                Z2 = 0.5*(ae*ye + Hn*yn)/(alphaij+Hij);
                Cij = alphaij/(2*(Hij+alphaij));
                Dij = -betaij/(4*(Hij + alphaij));
                Eij = Hij/(2*(Hij + alphaij));

                x_hatij = Cij*(x_new(i-1,j) + x_old(i+1,j)) + Dij*(x_new(i-1,j-1)+x_old(i+1,j+1)-x_old(i+1,j-1)-x_new(i-1,j+1)) + Eij*(x_new(i,j-1)+x_old(i,j+1));
                y_hatij = Cij*(y_new(i-1,j) + y_old(i+1,j)) + Dij*(y_new(i-1,j-1)+y_old(i+1,j+1)-y_old(i+1,j-1)-y_new(i-1,j+1)) + Eij*(y_new(i,j-1)+y_old(i,j+1)); 

                x_new(i,j) = w*x_hatij + (1-w)*x_old(i,j);
                y_new(i,j) = w*y_hatij + (1-w)*y_old(i,j);

            end
        end
    
    
    x_new(1,:) = x_new(N1,:);
    y_new(1,:) = y_new(N1,:);
    x_new(N1+1,:) = x_new(2,:);
    y_new(N1+1,:) = y_new(2,:);
    
    diffx = x_new - x_old;
    diffy = y_new - y_old;
    bool1 = norm(diffx,Inf) < tol;
    bool2 = norm(diffy,Inf) < tol;
    if (bool1 && bool2)
        iterations = k;
        break
    end    
    x_old = x_new;
    y_old = y_new;
    clf
    surface(x_new(1:N1,:),y_new(1:N1,:),u)
    pause(0.02)
    end

end





