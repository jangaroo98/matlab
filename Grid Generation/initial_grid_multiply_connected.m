function [BX,BY] = initial_grid_multiply_connected(BX,BY)
%Initial grid for multiply connected domains is built here.
[N1,N2] = size(BX);
x = BX(:,1);
y = BY(:,1);
sq = max(x.^2 + y.^2);
maxlen = sqrt(sq);
theta=fliplr(linspace(0,2*pi,N1));
for i=1:N1
    x0 = cos(theta(i))*maxlen;
    xn2 = BX(i,N2);
    y0 = sin(theta(i))*maxlen;
    yn2 = BY(i,N2);
    xlin = linspace(x0,xn2,N2);
    ylin = linspace(y0,yn2,N2);
    
    BX(i,2:N2) = xlin(2:N2);
    BY(i,2:N2) = ylin(2:N2);
end