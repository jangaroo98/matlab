function Helmholts_polar_abc(PPW,L)
r0 = 1;
R = 2;
k = 2*pi;
N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
N2 = ceil(PPW*(R-r0)*k/(2*pi));

r_linspace = linspace(r0,R,N2);
dr = (R-r0)/(N2-1);
theta_linspace = linspace(0,2 *pi,N1);
dt = 2*pi/(N1-1);

[R_grid,Theta_grid] = meshgrid(r_linspace,theta_linspace);

X = R_grid.*cos(Theta_grid);
Y = R_grid.*sin(Theta_grid);

uinc = @(x,y) exp(1j*k*x);
uinc_eval = uinc(X,Y);

size_A = (N1 - 1)*(N2-1 + 2*L);
% A = spalloc(size,size,(5*N1 - 16)*N2 + 18*L*N2);
% rhs = zeros(N1-1,N2-1 + 2*L);
% 
% 
% uinc = @(x,y) exp(1j*k*x);
% uinc_eval = uinc(X,Y);
% 
% j = 1;
% for i=1:N1-1
%     spot = (N1-1)*(j-1) + i;
%     A(spot,spot) = 1;
%     rhs(i,j) = - uinc_eval(i,j);
% end
% 
% for j=2:N2-2
%     r =  r_linspace(j);
%     alpha_plus = 1/dr^2 + 1/(2*dr * r);
%     alpha_minus = 1/dr^2 - 1/(2*dr * r);
%     alpha = k^2 - 2/(dr^2) - 2/(dt^2 * r^2);
%     beta = 1/(dt^2 * r^2);
%     for i=1:N1-1
%         spot = (N1-1)*(j-1) + i;
%         [super_diag,sub_diag] = super_sub(i,N1);
% 
%         A(spot,spot - (N1-1)) = alpha_minus;
%         
% 
%         A(spot,spot + sub_diag) = beta; 
%         A(spot,spot) = alpha;
%         A(spot,spot + super_diag) = beta;
% 
%         
%         A(spot,spot + (N1-1)) = alpha_plus;       
%     end
% end
% 
% j = N2-1;
% r =  r_linspace(j);
% alpha_plus = 1/dr^2 + 1/(2*dr * r);
% alpha_minus = 1/dr^2 - 1/(2*dr * r);
% alpha = k^2 - 2/(dr^2) - 2/(dt^2 * r^2);
% beta = 1/(dt^2 * r^2);
% kR = k*R;
% H0 = besselh(0,kR);
% H1 = besselh(1,kR);
% 
% for i=1:N1-1
%     spot = (N1-1)*(j-1) + i;
%     base = spot + N1 - 1;
%     [super_diag,sub_diag] = super_sub(i,N1);
%     
%     A(spot,spot - (N1-1)) = alpha_minus;
%            
%     A(spot,spot + sub_diag) = beta;
%     A(spot,spot) = alpha;
%     A(spot,spot + super_diag) = beta;        
%     
%     
%     for l =0:L-1
%         A(spot,base + 2*(N1-1)*l) = alpha_plus*H0/((kR)^l);
%         A(spot,base + N1-1 + 2*(N1 -1)*l) = alpha_plus*H1/((kR)^l);
% %         A(spot,base + 2*(N1 -1)*l) = alpha_plus*besselh(0,k*R)/((R)^l);
% %         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = alpha_plus*besselh(1,k*R)/((R)^l);
%     end
% end
%     
% 
% j = N2;
% r =  r_linspace(j);
% alpha_plus = 1/dr^2 + 1/(2*dr * r);
% alpha_minus = 1/dr^2 - 1/(2*dr * r);
% alpha = k^2 - 2/(dr^2) - 2/(dt^2 * r^2);
% beta = 1/(dt^2 * r^2);
% 
% 
% for i=1:N1-1
%     spot = (N1-1)*(j-1) + i;
%     base = spot;
%     A(spot,spot -(N1-1)) = alpha_minus+alpha_plus;
%     [super_diag,sub_diag] = super_sub(i,N1);
%     
%     for l=0:L-1
%         Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
%         Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
%         Cl = -alpha_plus*2*dr*Al + alpha*H0/((kR)^l);
%         Dl = -alpha_plus*2*dr*Bl + alpha*H1/((kR)^l);
%          
%         A(spot,base + super_diag + 2*(N1 -1)*l) = beta*H0/((kR)^l);
%         A(spot,base + 2*(N1 -1)*l) = Cl;
%         A(spot,base + sub_diag + 2*(N1 -1)*l) = beta*H0/((kR)^l);
%         
%         A(spot,base + super_diag + (N1-1) + 2*(N1 -1)*l) = beta*H1/((kR)^l);
%         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Dl;
%         A(spot,base + sub_diag + (N1-1) + 2*(N1 -1)*l) = beta*H1/((kR)^l);
%         
%     end
% end
% 
% j = N2 + 1;
% 
% 
% for i=1:N1-1
%     spot = (N1-1)*(j-1) + i;
% %     base = spot - (N1-1);
% %     A(spot,spot -2*(N1-1)) = 2/(dr^2);
% %     
% %     for l=0:L-1
% %         Al = k*H1/((kR)^l) + k*l*H0/((k*R)^(l+1));
% %         Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
% %         Ml = -2*Al/dr + k^2 * (-H0/((kR)^l) - (2*l + 1)*H1/((kR)^(l+1)) + (l+1)*l*H0/((kR)^(l+2))) - 2*H0/(dr^2 * (kR)^(l));
% %         Nl = -2*Bl/dr + k^2 * ((2*l + 1)*H0/((kR)^(l+1)) + H1/((kR)^l) - (l+1)*(l+2)*H1/(kR)^(l+2)) - 2*H1/(dr^2 * (kR)^l);
% %         A(spot,base + 2*(N1 -1)*l) = Ml;
% %         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Nl;
% %     end
% 
%     base = spot  + (N1-1) + (L-2)*2*(N1-1);
%     [super_diag,sub_diag] = super_sub(i,N1);
%     
%     A(spot,base + super_diag) = H0/(dt^2);
%     A(spot,base) = H0*(((L-1)^2)-(2/(dt^2)));
%     A(spot,base + sub_diag) = H0/(dt^2);
%     
%     A(spot,base + super_diag + (N1-1)) = H1/(dt^2);
%     A(spot,base + (N1-1)) = H1*(L^2-(2/(dt^2)));
%     A(spot,base + sub_diag + (N1-1)) = H1/(dt^2);
%   
%   
% end
% 
% for l=1:L-1
%     
%     j = N2 + 2 + 2*(l-1);
%     
%     for i=1:N1-1
%         spot = (N1-1)*(j-1) + i;
%         base = spot - (N1-1);
%         [super_diag,sub_diag] = super_sub(i,N1);
%         A(spot,spot) = 2*l;
%         A(spot,base) = - (-l^2 + 2/dt^2);
%         
%         A(spot,base + sub_diag) = 1/dt^2;
%         A(spot,base + super_diag) = 1/dt^2;
%     end
%     
%     j = N2 + 3 + 2*(l-1);
%     for i=1:N1-1
%         spot = (N1-1)*(j-1) + i;
%         base = spot - 3*(N1-1);
%         [super_diag,sub_diag] = super_sub(i,N1);
%         
%         A(spot,spot) = 2*l;
%         A(spot,base) = - ((l-1)^2 - 2/dt^2);
%         
%         A(spot,base + sub_diag) = -1/dt^2;
%         A(spot,base + super_diag) = -1/dt^2;
%         
%     end
% end
kR = k*R;
H0 = besselh(0,kR);
H1 = besselh(1,kR);

rhs = zeros(N1-1,N2-1 + 2*L);
j = 1;
for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    rhs(i,j) = - uinc_eval(i,j);
end

A = Helmholts_polar_coefficient_matrix(X,Y,L,k);
spy(A)
b = reshape(rhs,size_A,1);
u = A\b;
u_vec = u(1:(N1-1)*(N2-1));
far = u((N1-1)*(N2-1) + 1:size_A);
far = reshape(far,N1-1,[]);
f = far(1:N1-1,1:2:2*L);
g = far(1:N1-1,2:2:2*L);

ls = 0:L-1;
broadcast = arrayfun(@(l) (kR)^(-l),ls);
UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);


u_vec = [u_vec;UN2];
u_soln = reshape(u_vec,N1-1,N2);
u_soln = [u_soln ; u_soln(1,1:N2)];
figure
surface(X,Y,abs(u_soln + uinc_eval))
title('approximated solution')



end

function [super_diag,sub_diag] = super_sub(i,N1)
if i == 1
    sub_diag = (N1-1) - 1;
    super_diag = 1;

elseif i == N1-1 
    sub_diag = - 1;
    super_diag = - (N1-1) + 1;

else
    sub_diag = -1;
    super_diag =  1;
end    
end
