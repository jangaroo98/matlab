function laplace_polar

N1 = 101;
N2 = 101;

r0 = 1;
R = 3;

r_linspace = linspace(r0,R,N2);
dr = (R-r0)/(N2-1);
theta_linspace = linspace(0,2 *pi,N1);
dt = 2*pi/(N1-1);

[R_grid,Theta_grid] = meshgrid(r_linspace,theta_linspace);


X = R_grid.*cos(Theta_grid);
Y = R_grid.*sin(Theta_grid);

size = (N1 - 1)*(N2);
A = spalloc(size,size,(5*N1 - 16)*N2);
rhs = zeros(N1-1,N2);

k = -2*pi;
uinc = @(x,y) exp(1j*k*x);
uinc_eval = uinc(X,Y);

j = 1;
for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    A(spot,spot) = 1;
    rhs(i,j) = 3;
end

for j=2:N2-1
    r =  R_grid(j);
    alpha_plus = 1/dr^2 + 1/(2*dr * r);
    alpha_minus = 1/dr^2 - 1/(2*dr * r);
    alpha = - 2/(dr^2) - 2/(dt^2 * r^2);
    beta = 1/(dt^2 * r^2);
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
         
        if i == 1
            sub_diag = spot + (N1-1) - 1;
            sup_diag = spot + 1;
            
        elseif i == N1-1 
            sub_diag = spot - 1;
            sup_diag = spot - (N1-1) + 1;
            
        else
            sub_diag = spot - 1;
            sup_diag = spot + 1;
        end

        A(spot,spot - (N1-1)) = alpha_minus;
        

        A(spot,sub_diag) = beta; 
        A(spot,spot) = alpha;
        A(spot,sup_diag) = beta;

        
        A(spot,spot + (N1-1)) = alpha_plus;
        
        
    end
end
j = N2;
for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    A(spot,spot) = 1;
    rhs(i,j) = 0;
end
b = reshape(rhs,size,1);
u = A\b;
u_soln = reshape(u,N1-1,N2);
u_soln = [u_soln ; u_soln(1,1:N2)];
figure
surface(X,Y,abs(u_soln))
end