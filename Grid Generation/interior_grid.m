function [IX,IY] = interior_grid(N1,N2,simplyconnected)
%Initial grid for simply connected domain is built here.
stepx = 1/(N1-1);
stepy = 1/(N2-1);
IX = zeros(1,1);
IY = zeros(1,1);
for i = 1:N1-2
    for j = 1:N2-2
        IX(i,j) = (i)*stepx;
        IY(i,j) = (j)*stepy;
    end
end