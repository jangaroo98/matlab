function[X,Y,uscat,uinc_eval,ffp,dt] =  Helmholts_curvilinear_abc(PPW,L,w,tol,region,show)

%solves the helmholts equation laplacian(u) = -k^2 u on the domain region
%in curvilinear coordinates.
%PPW is points per wavelength, which is used to calculate N1, N2.
%N1 is number of points in theta. N2 is number of points in radial.
%w is the successive overrelaxation parameter for grid generation
%tol is the tolerance for grid generation
%region is the shape of the region. See boundary_of_region
k = 2*pi;
z = 0; %acoustic hardness

%Different region will have different r0. These are given here. 
if region=='roid3'
    r0 = 0.5002;
elseif region=='leaf3'
    r0 = 0.4520;
else
    r0 = 1;
end
%for now, rmax is the same for every region. 
rmax = 3;

%calculate N1,N2 and create the grid using the elliptic polar algorithm
N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
N2 = ceil(PPW*(rmax-r0)*k/(2*pi));

fprintf('\n **********************************************\n');
fprintf(['\nGenerating Curvilinear Grid for PPW  = ' num2str(PPW) ', for region = ' region '\n']);
tic;
[X,Y] = elliptic_polar(N1,N2,w,tol,10000,region);
toc
fprintf('\n **********************************************\n');

%Convert grid to polar coordinates to be used in calulating the true
%solution
R = sqrt(X.^2 + Y.^2);
T = to_polar(X,Y);


%initializa sparse matrix and the right hand side vector
numnonzero = (N1-1)*(20*L - 21 +9*N2);
size = (N1 - 1)*(N2-1 + 2*L);
A = spalloc(size,size,numnonzero); 
rhs = zeros(N1-1,N2-1 + 2*L);

%these are important constants
r = sqrt(X(:,N2-1).^2 + Y(:,N2-1).^2); %the radius of the N2-1 level
R = sqrt(X(:,N2).^2 + Y(:,N2).^2); %the outside radius (3)
R_ = mean(R);
DR = R-r;  %a vector containing delta r for the N2 level
dt = 2*pi/(N1-1); %delta theta


%incident wave
theta = 0; %incident wave angle;
uinc = @(x,y) exp(1j*k*(x*cos(theta) + y*sin(theta)));
uinc_eval = uinc(X,Y);


%Fill the matrix
tic;
fprintf('Constructing Coefficient Matrix... \n')
%First, deal with the inner boundary conditions. Adds N1-1 nz terms
j = 1;
for i=1:N1-1
    
    [super_diag,sub_diag] = super_sub(i,N1);
    xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
    xn = (-3*X(i,1) + 4*X(i,2) - X(i,3))/2;
    ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
    yn = (-3*Y(i,1) + 4*Y(i,2) - Y(i,3))/2;
    xne = (-3*X(i+super_diag,1)+4*X(i+super_diag,2)-X(i+super_diag,3)+3*X(i+sub_diag,1)-4*X(i+sub_diag,2)+X(i+sub_diag,3))/4;
    yne = (-3*Y(i+super_diag,1)+4*Y(i+super_diag,2)-Y(i+super_diag,3)+3*Y(i+sub_diag,1)-4*Y(i+sub_diag,2)+Y(i+sub_diag,3))/4;

    %Importand constants for curvillinear coordinates
    alpha = xn^2 + yn^2;
    beta = xe*xn + ye*yn;
    gamma = xe^2 + ye^2;
    J = xe*yn - xn*ye;
    ae = 2*(xn*xne + yn*yne); 
    gn = 2*(xe*xne + ye*yne); 
    mu = ye*cos(theta) - xe*sin(theta);
    spot = (N1-1)*(j-1) + i;
    
    A(spot,spot) = -z*3*gamma + 2*(1-z)*sqrt(gamma)*J;
    A(spot,spot + (N1-1)) = z*4*gamma;
    A(spot,spot + 2*(N1-1)) = -z*gamma;
    
    A(spot,spot + sub_diag) = z*beta;
    A(spot,spot + super_diag) = -z*beta;
    
    rhs(i,1) =  2*(1j*k*z*mu - (1-z)*sqrt(gamma))*J*uinc_eval(i,1); % inner boundary condition
end


%Interior Points. Adds (N1-1)*9*(N2-3) nz terms
for j=2:N2-2
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        [super_diag,sub_diag] = super_sub(i,N1);
        [alpha,beta,gamma,J,ae,gn] = important_constants(i,j,X,Y,super_diag,sub_diag);

        %fill the matrix 
        A(spot,spot - (N1-1) +sub_diag) = -beta/2;
        A(spot,spot - (N1-1)) = gamma - gn/4;
        A(spot,spot - (N1-1) + super_diag) = beta/2;

        A(spot,spot+sub_diag) = alpha - ae/4; 
        A(spot,spot) = (J^2)*k^2 -2*gamma - 2*alpha;
        A(spot,spot+super_diag) = alpha + ae/4;

        A(spot,spot + (N1-1) +sub_diag) = beta/2;
        A(spot,spot + (N1-1)) = gamma + gn/4;
        A(spot,spot + (N1-1) + super_diag) = -beta/2;

    end
end

%Next is the N2-1 level. This is a special case. Adds (N1-1)*(6 + 6*L) nz
%terms
j = N2-1;


R_ = mean(sqrt(X(:,N2).^2 + Y(:,N2).^2));
kR = k*R_;
H0 = besselh(0,kR);
H1 = besselh(1,kR);

for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    base = spot + N1 - 1;
    
    [super_diag,sub_diag] = super_sub(i,N1);
    [alpha,beta,gamma,J,ae,gn] = important_constants(i,j,X,Y,super_diag,sub_diag);
    
    
    A(spot,spot - (N1-1) +sub_diag) = -beta/2;
    A(spot,spot - (N1-1)) = gamma - gn/4;
    A(spot,spot - (N1-1) + super_diag) = beta/2;

    A(spot,spot+sub_diag) = alpha - ae/4; 
    A(spot,spot) = (J^2)*k^2 -2*gamma - 2*alpha;
    A(spot,spot+super_diag) = alpha + ae/4;    
    
    
    for l =0:L-1
        A(spot,base + 2*(N1-1)*l + sub_diag) = (beta/2)*H0/((kR)^l);
        A(spot,base + 2*(N1-1)*l) = (gamma + gn/4)*H0/((kR)^l);
        A(spot,base + 2*(N1-1)*l + super_diag) = (-beta/2)*H0/((kR)^l);
        
        
        A(spot,base + N1-1 + 2*(N1 -1)*l + sub_diag) = (beta/(2))*H1/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l) = (gamma + gn/4)*H1/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l + super_diag) = (-beta/2)*H1/((kR)^l);
    end
end
    
%now, for the artificial boundary. This uses the continuity of the first
%derivative. Adds (N1-1)*(1 + 6*L) nz terms
j = N2;


for i=1:N1-1
    [super_diag,sub_diag] = super_sub(i,N1);
    
    %we need to use second order one sided difference to get the
    %derivatives
    xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
    xn = (3*X(i,N2) - 4*X(i,N2-1) + X(i,N2-2))/2;
    ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
    yn = (3*Y(i,N2) - 4*Y(i,N2-1) + Y(i,N2-2))/2;
    xne = (3*X(i+super_diag,N2)-4*X(i+super_diag,N2-1)+X(i+super_diag,N2-2)-3*X(i+sub_diag,N2)+4*X(i+sub_diag,N2-1)-X(i+sub_diag,N2-2))/4;
    yne = (3*Y(i+super_diag,N2)-4*Y(i+super_diag,N2-1)+Y(i+super_diag,N2-2)-3*Y(i+sub_diag,N2)+4*Y(i+sub_diag,N2-1)-Y(i+sub_diag,N2-2))/4;

    %Importand constants for curvillinear coordinates
    alpha = xn^2 + yn^2;
    beta = xe*xn + ye*yn;
    gamma = xe^2 + ye^2;
    J = xe*yn - xn*ye;
    ae = 2*(xn*xne + yn*yne); 
    gn = 2*(xe*xne + ye*yne); 
    
    dr = DR(i);
%     dr = mean(DR);
    spot = (N1-1)*(j-1) + i;
    base = spot;
    
    a_ = (J^2)*k^2 -2*(gamma) - 2*alpha;
    b_ = alpha - ae/(4);
    c_ = alpha + ae/(4);
    d_ = gamma - gn/(4);
    e_ = gamma + gn/(4);
    f_ = 2*beta/(4);
    
    %fill the matrix
    A(spot,spot -(N1-1)) = d_+e_;
    for l=0:L-1
        Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
        Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
         
        A(spot,base + super_diag + 2*(N1 -1)*l) = c_*H0/kR^l + 2*f_*dr*Al;
        A(spot,base + 2*(N1 -1)*l) = a_*H0/kR^l - 2*e_*dr*Al;
        A(spot,base + sub_diag + 2*(N1 -1)*l) = b_*H0/kR^l - 2*f_*dr*Al;
        
        A(spot,base + super_diag + (N1-1) + 2*(N1 -1)*l) = c_*H1/kR^l + 2*f_*dr*Bl;
        A(spot,base + N1 - 1 + 2*(N1 -1)*l) =a_*H1/kR^l - 2*e_*dr*Bl;
        A(spot,base + sub_diag + (N1-1) + 2*(N1 -1)*l) = b_*H1/kR^l - 2*f_*dr*Bl;
        
    end
end

%This next set of equations is for the truncated helmholts continuity. The
%commented out code is the alternate continuity of the second derivative
%condition. Truncated Helmholts adds (N1-1)*6 nz terms
j = N2 + 1;


for i=1:N1-1
%     spot = (N1-1)*(j-1) + i;
%     base = spot - (N1-1);
%     A(spot,spot -2*(N1-1)) = 2/(dr^2);
%     
%     for l=0:L-1
%         Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
%         Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
%         Ml = -2*Al/dr + k^2 * (-H0/((kR)^l) - (2*l + 1)*H1/((kR)^(l+1)) + (l+1)*l*H0/((kR)^(l+2))) - 2*H0/(dr^2 * (kR)^(l));
%         Nl = -2*Bl/dr + k^2 * ((2*l + 1)*H0/((kR)^(l+1)) + H1/((kR)^l) - (l+1)*(l+2)*H1/(kR)^(l+2)) - 2*H1/(dr^2 * (kR)^l);
%         A(spot,base + 2*(N1 -1)*l) = Ml;
%         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Nl;
%     end
    spot = (N1-1)*(j-1) + i;
    base = spot  + (N1-1) + (L-2)*2*(N1-1);
    [super_diag,sub_diag] = super_sub(i,N1);
    
    A(spot,base + super_diag) = H0/(dt^2);
    A(spot,base) = H0*(((L-1)^2)-(2/(dt^2)));
    A(spot,base + sub_diag) = H0/(dt^2);
    
    A(spot,base + super_diag + (N1-1)) = H1/(dt^2);
    A(spot,base + (N1-1)) = H1*(L^2-(2/(dt^2)));
    A(spot,base + sub_diag + (N1-1)) = H1/(dt^2);
  
  
end

%recursive equations for the karp expansion equations. 
%Adds (L-1)*((N1-1)*8) nz terms.  
for l=1:L-1
    
    j = N2 + 2 + 2*(l-1);
    
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - (N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        A(spot,spot) = 2*l;
        A(spot,base) = - (-l^2 + 2/dt^2);
        
        A(spot,base + sub_diag) = 1/dt^2;
        A(spot,base + super_diag) = 1/dt^2;
    end
    
    j = N2 + 3 + 2*(l-1);
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - 3*(N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        
        A(spot,spot) = 2*l;
        A(spot,base) = - ((l-1)^2 - 2/dt^2);
        
        A(spot,base + sub_diag) = -1/dt^2;
        A(spot,base + super_diag) = -1/dt^2;
        
    end
end


fprintf('\nMatrix Condition Number: %d\n',condest(A));
toc
fprintf('\n **********************************************\n');


b = reshape(rhs,size,1);
fprintf('Solving System\n')
tic;
u = A\b;
toc
fprintf('\n **********************************************\n');
u_vec = u(1:(N1-1)*(N2-1));

far = u((N1-1)*(N2-1) + 1:size);
far = reshape(far,N1-1,[]);
f = far(1:N1-1,1:2:2*L);
g = far(1:N1-1,2:2:2*L);





ls = 0:L-1;
broadcast = arrayfun(@(l) (kR)^(-l),ls);
UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);


u_vec = [u_vec;UN2];
uscat = reshape(u_vec,N1-1,N2);
uscat = [uscat ; uscat(1,1:N2)];

    

u_total_approx = abs(uscat + uinc_eval);
if show
figure;
surface(X,Y,u_total_approx)
title(['approximation of total amplitude for ' region ', PPW =' num2str(PPW)])
shading interp;
end

spy(A)



F0 = [f(1:N1-1,1) ; f(1,1)];
G0 = [g(1:N1-1,1) ; g(1,1)];

imax = round((N1-1)/2);
n = 0;
c_m = zeros(1,imax+imax+1);
for mm = -imax:imax-1
    n = n + 1;
    for j = 1:N1-1
        c_m(n) = c_m(n) + uscat(j,N2)*exp(-1j*mm*T(j,N2));
    end
    c_m(n) = (1/(N1-1))*c_m(n);
    b_m(n) = c_m(n)/besselh(mm,kR); %was r(N1)?
end

A0 = zeros((N1-1)+1,1);
coeff = sqrt(2/(pi*k))*exp(-1j*pi/4);
for j = 1:(N1-1)
    n = 0;
    for mm = -imax:imax-1
        n = n + 1;
        A0(j) = A0(j) + b_m(n) * (-1j)^mm * exp(1j*mm*T(j,N2));
    end
    A0(j) = coeff * A0(j);
end
A0((N1-1)+1) = A0(1);
ffp = A0;
if show
figure
polarplot(T(:,N2),abs(ffp))
title(['approximation of farfield pattern for ' region ', PPW =' num2str(PPW)])
end
end

function [super_diag,sub_diag] = super_sub(i,N1)
%this helper function returns the location of the super/sub diagonal
%relative to the main diagonal. 

if i ~= 1 && i ~= N1-1
    sub_diag = -1;
    super_diag =  1;
    
elseif i == N1-1 
    sub_diag = - 1;
    super_diag = - (N1-1) + 1;

else
    sub_diag = (N1-1) - 1;
    super_diag = 1;
end    
end

function theta = to_polar(X,Y)

theta = zeros(size(X),'like',X);
[n,m] = size(X);
for i=1:n
    for j=1:m
        x = X(i,j);
        y = Y(i,j);
        thet = atan(y/x);
        if x<0
            thet = thet + pi;
        end
        theta(i,j) = thet;
    end
end

end

function [alpha,beta,gamma,J,ae,gn]=important_constants(i,j,X,Y,super_diag,sub_diag) 
%take derivatives and partial derivatives
xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
xn = 0.5*(X(i,j+1) - X(i,j-1));
ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
yn = 0.5*(Y(i,j+1) - Y(i,j-1));
xne = 0.25*(X(i+sub_diag,j-1)+X(i+super_diag,j+1)-X(i+super_diag,j-1)-X(i+sub_diag,j+1));
yne = 0.25*(Y(i+sub_diag,j-1)+Y(i+super_diag,j+1)-Y(i+super_diag,j-1)-Y(i+sub_diag,j+1));

%Importand constants for curvillinear coordinates
alpha = xn^2 + yn^2;
beta = xe*xn + ye*yn;
gamma = xe^2 + ye^2;
J = xe*yn - xn*ye;
ae = 2*(xn*xne + yn*yne); 
gn = 2*(xe*xne + ye*yne); 
end