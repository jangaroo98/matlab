function[X,Y,uscat,uinc_eval,ffp,dt] =  Helmholts_curvilinear_abc_copy(PPW,L,w,tol,region)

%solves the helmholts equation laplacian(u) = -k^2 u on the domain region
%in curvilinear coordinates.


%N1 is how many points in theta
%N2 is how many radial points
%w is the successive overrelaxation parameter for grid generation
%tol is the tolerance for grid generation
%region is the shape of the region. See boundary_of_region
k = 2*pi;
r0 = 1;
rmax = 3;
N1 = ceil(PPW*(2*pi*r0)*k/(2*pi));
N2 = ceil(PPW*(rmax-r0)*k/(2*pi));
[X,Y] = elliptic_polar(N1,N2,w,tol,10000,region);
R = sqrt(X.^2 + Y.^2);
T = to_polar(X,Y);


%initializa sparse matrix and the right hand side vector
size = (N1 - 1)*(N2-1 + 2*L);
A = spalloc(size,size,(5*N1 - 16)*N2 + 18*L*N2);
rhs = zeros(N1-1,N2-1 + 2*L);

%these are important constants
r = sqrt(X(:,N2-1).^2 + Y(:,N2-1).^2);
R = sqrt(X(:,N2).^2 + Y(:,N2).^2);
R_ = mean(R);
DR = R-r;
dt = 2*pi/(N1-1);


%incident wave
uinc = @(x,y) exp(1j*k*x);
uinc_eval = uinc(X,Y);
% figure
% surface(X,Y,abs(real(uinc_eval)))


for i=1:N1-1
    A(i,i) = 1;
    rhs(i,1) =  -uinc_eval(i,1); % inner boundary condition
end


for j=2:N2-2
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        [super_diag,sub_diag] = super_sub(i,N1);
        [alpha,beta,gamma,J,ae,gn] = important_constants(i,j,X,Y,super_diag,sub_diag);

        %fill the matrix 
        A(spot,spot - (N1-1) +sub_diag) = -2*beta/(4);
        A(spot,spot - (N1-1)) = gamma - gn/(4);
        A(spot,spot - (N1-1) + super_diag) = 2*beta/(4);

        A(spot,spot+sub_diag) = alpha - ae/(4); 
        A(spot,spot) = (J^2)*k^2 -2*(gamma) - 2*alpha;
        A(spot,spot+super_diag) = alpha + ae/(4);

        A(spot,spot + (N1-1) +sub_diag) = 2*beta/(4);
        A(spot,spot + (N1-1)) = gamma + gn/(4);
        A(spot,spot + (N1-1) + super_diag) = -2*beta/(4);

    end
end


j = N2-1;


R_ = mean(sqrt(X(:,N2).^2 + Y(:,N2).^2));
kR = k*R_;
H0 = besselh(0,kR);
H1 = besselh(1,kR);

for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
    base = spot + N1 - 1;
    
    [super_diag,sub_diag] = super_sub(i,N1);
    [alpha,beta,gamma,J,ae,gn] = important_constants(i,j,X,Y,super_diag,sub_diag);
    
    
    A(spot,spot - (N1-1) +sub_diag) = -2*beta/(4);
    A(spot,spot - (N1-1)) = gamma - gn/(4);
    A(spot,spot - (N1-1) + super_diag) = 2*beta/(4);

    A(spot,spot+sub_diag) = alpha - ae/(4); 
    A(spot,spot) = (J^2) * k^2 -2*(gamma) - 2*alpha;
    A(spot,spot+super_diag) = alpha + ae/(4);    
    
    
    for l =0:L-1
        A(spot,base + 2*(N1-1)*l + sub_diag) = (2*beta/(4))*H0/((kR)^l);
        A(spot,base + 2*(N1-1)*l) = (gamma + gn/(4))*H0/((kR)^l);
        A(spot,base + 2*(N1-1)*l + super_diag) = (-2*beta/(4))*H0/((kR)^l);
        
        
        A(spot,base + N1-1 + 2*(N1 -1)*l + sub_diag) = (2*beta/(4))*H1/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l) = (gamma + gn/(4))*H1/((kR)^l);
        A(spot,base + N1-1 + 2*(N1 -1)*l + super_diag) = (-2*beta/(4))*H1/((kR)^l);
    end
end
    

j = N2;


for i=1:N1-1
    [super_diag,sub_diag] = super_sub(i,N1);
    xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
    xn = (3*X(i,N2) - 4*X(i,N2-1) + X(i,N2-2))/2;
    ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
    yn = (3*Y(i,N2) - 4*Y(i,N2-1) + Y(i,N2-2))/2;
    xne = (3*X(i+super_diag,N2)-4*X(i+super_diag,N2-1)+X(i+super_diag,N2-2)-3*X(i+sub_diag,N2)+4*X(i+sub_diag,N2-1)-X(i+sub_diag,N2-2))/4;
    yne = (3*Y(i+super_diag,N2)-4*Y(i+super_diag,N2-1)+Y(i+super_diag,N2-2)-3*Y(i+sub_diag,N2)+4*Y(i+sub_diag,N2-1)-Y(i+sub_diag,N2-2))/4;

    %Importand constants for curvillinear coordinates
    alpha = xn^2 + yn^2;
    beta = xe*xn + ye*yn;
    gamma = xe^2 + ye^2;
    J = xe*yn - xn*ye;
    ae = 2*(xn*xne + yn*yne); 
    gn = 2*(xe*xne + ye*yne); 
    
    dr = DR(i);
%     dr = mean(DR);
    spot = (N1-1)*(j-1) + i;
    base = spot;
    
    a_ = (J^2)*k^2 -2*(gamma) - 2*alpha;
    b_ = alpha - ae/(4);
    c_ = alpha + ae/(4);
    d_ = gamma - gn/(4);
    e_ = gamma + gn/(4);
    f_ = 2*beta/(4);


    A(spot,spot -(N1-1)) = d_+e_;
    
    
    for l=0:L-1
        Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
        Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
         
        A(spot,base + super_diag + 2*(N1 -1)*l) = c_*H0/kR^l + 2*f_*dr*Al;
        A(spot,base + 2*(N1 -1)*l) = a_*H0/kR^l - 2*e_*dr*Al;
        A(spot,base + sub_diag + 2*(N1 -1)*l) = b_*H0/kR^l - 2*f_*dr*Al;
        
        A(spot,base + super_diag + (N1-1) + 2*(N1 -1)*l) = c_*H1/kR^l + 2*f_*dr*Bl;
        A(spot,base + N1 - 1 + 2*(N1 -1)*l) =a_*H1/kR^l - 2*e_*dr*Bl;
        A(spot,base + sub_diag + (N1-1) + 2*(N1 -1)*l) = b_*H1/kR^l - 2*f_*dr*Bl;
        
    end
end

j = N2 + 1;


for i=1:N1-1
    spot = (N1-1)*(j-1) + i;
%     base = spot - (N1-1);
%     A(spot,spot -2*(N1-1)) = 2/(dr^2);
%     
%     for l=0:L-1
%         Al = k*H1/((kR)^l) + k*l*H0/((kR)^(l+1));
%         Bl = k*(l+1)*H1/((kR)^(l+1)) - k*H0/((kR)^l);
%         Ml = -2*Al/dr + k^2 * (-H0/((kR)^l) - (2*l + 1)*H1/((kR)^(l+1)) + (l+1)*l*H0/((kR)^(l+2))) - 2*H0/(dr^2 * (kR)^(l));
%         Nl = -2*Bl/dr + k^2 * ((2*l + 1)*H0/((kR)^(l+1)) + H1/((kR)^l) - (l+1)*(l+2)*H1/(kR)^(l+2)) - 2*H1/(dr^2 * (kR)^l);
%         A(spot,base + 2*(N1 -1)*l) = Ml;
%         A(spot,base + N1 - 1 + 2*(N1 -1)*l) = Nl;
%     end

    base = spot  + (N1-1) + (L-2)*2*(N1-1);
    [super_diag,sub_diag] = super_sub(i,N1);
    
    A(spot,base + super_diag) = H0/(dt^2);
    A(spot,base) = H0*(((L-1)^2)-(2/(dt^2)));
    A(spot,base + sub_diag) = H0/(dt^2);
    
    A(spot,base + super_diag + (N1-1)) = H1/(dt^2);
    A(spot,base + (N1-1)) = H1*(L^2-(2/(dt^2)));
    A(spot,base + sub_diag + (N1-1)) = H1/(dt^2);
  
  
end

for l=1:L-1
    
    j = N2 + 2 + 2*(l-1);
    
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - (N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        A(spot,spot) = 2*l;
        A(spot,base) = - (-l^2 + 2/dt^2);
        
        A(spot,base + sub_diag) = 1/dt^2;
        A(spot,base + super_diag) = 1/dt^2;
    end
    
    j = N2 + 3 + 2*(l-1);
    for i=1:N1-1
        spot = (N1-1)*(j-1) + i;
        base = spot - 3*(N1-1);
        [super_diag,sub_diag] = super_sub(i,N1);
        
        A(spot,spot) = 2*l;
        A(spot,base) = - ((l-1)^2 - 2/dt^2);
        
        A(spot,base + sub_diag) = -1/dt^2;
        A(spot,base + super_diag) = -1/dt^2;
        
    end
end
%spy(A)
b = reshape(rhs,size,1);
u = A\b;
u_vec = u(1:(N1-1)*(N2-1));

far = u((N1-1)*(N2-1) + 1:size);
far = reshape(far,N1-1,[]);
f = far(1:N1-1,1:2:2*L);
g = far(1:N1-1,2:2:2*L);





ls = 0:L-1;
broadcast = arrayfun(@(l) (kR)^(-l),ls);
UN2 = sum(H0*f.*broadcast + H1*g.*broadcast,2);


u_vec = [u_vec;UN2];
uscat = reshape(u_vec,N1-1,N2);
uscat = [uscat ; uscat(1,1:N2)];
figure;
u_total_approx = abs(uscat + uinc_eval);
surface(X,Y,u_total_approx)
title(['approximation, PPW =' num2str(PPW)])






F0 = [f(1:N1-1,1) ; f(1,1)];
G0 = [g(1:N1-1,1) ; g(1,1)];

imax = round((N1-1)/2);
n = 0;
c_m = zeros(1,imax+imax+1);
for mm = -imax:imax-1
    n = n + 1;
    for j = 1:N1-1
        c_m(n) = c_m(n) + uscat(j,N2)*exp(-1j*mm*T(j,N2));
    end
    c_m(n) = (1/(N1-1))*c_m(n);
    b_m(n) = c_m(n)/besselh(mm,kR); %was r(N1)?
end

A0 = zeros((N1-1)+1,1);
coeff = sqrt(2/(pi*k))*exp(-1j*pi/4);
for j = 1:(N1-1)
    n = 0;
    for mm = -imax:imax-1
        n = n + 1;
        A0(j) = A0(j) + b_m(n) * (-1j)^mm * exp(1j*mm*T(j,N2));
    end
    A0(j) = coeff * A0(j);
end
A0((N1-1)+1) = A0(1);
ffp = A0;
figure
polarplot(T(:,N2),abs(ffp))
end

function [super_diag,sub_diag] = super_sub(i,N1)
%this helper function returns the location of the super/sub diagonal
%relative to the main diagonal. 

if i ~= 1 && i ~= N1-1
    sub_diag = -1;
    super_diag =  1;
    
elseif i == N1-1 
    sub_diag = - 1;
    super_diag = - (N1-1) + 1;

else
    sub_diag = (N1-1) - 1;
    super_diag = 1;
end    
end

function theta = to_polar(X,Y)

theta = zeros(size(X),'like',X);
[n,m] = size(X);
for i=1:n
    for j=1:m
        x = X(i,j);
        y = Y(i,j);
        thet = atan(y/x);
        if x<0
            thet = thet + pi;
        end
        theta(i,j) = thet;
    end
end

end

function [alpha,beta,gamma,J,ae,gn]=important_constants(i,j,X,Y,super_diag,sub_diag) 
%take derivatives and partial derivatives
xe = 0.5*(X(i+super_diag,j) - X(i+sub_diag,j));
xn = 0.5*(X(i,j+1) - X(i,j-1));
ye = 0.5*(Y(i+super_diag,j) - Y(i+sub_diag,j));
yn = 0.5*(Y(i,j+1) - Y(i,j-1));
xne = 0.25*(X(i+sub_diag,j-1)+X(i+super_diag,j+1)-X(i+super_diag,j-1)-X(i+sub_diag,j+1));
yne = 0.25*(Y(i+sub_diag,j-1)+Y(i+super_diag,j+1)-Y(i+super_diag,j-1)-Y(i+sub_diag,j+1));

%Importand constants for curvillinear coordinates
alpha = xn^2 + yn^2;
beta = xe*xn + ye*yn;
gamma = xe^2 + ye^2;
J = xe*yn - xn*ye;
ae = 2*(xn*xne + yn*yne); 
gn = 2*(xe*xne + ye*yne); 
end