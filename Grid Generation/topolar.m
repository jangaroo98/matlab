function [R,Theta] = topolar(X,Y)

% Theta = zeros(size(X),'like',X);
% [n,m] = size(X);
% v = [1 0 0];
% for i=1:n
%     for j=1:m
%         x = X(i,j);
%         y = Y(i,j);
%         u = [x y 0];
% %         thet = atan2(norm(cross(u,v)),dot(u,v));
% % 
% %         if y < 0
% %             thet = 2*pi - thet;
% %         end
%         thet = atan2(y,x);
% 
%         if thet < 0
%             thet = 2*pi + thet;
%         end
% 
%         
%         
%         Theta(i,j) = thet;
%     end
% end

Theta = atan2(Y,X);
Theta = Theta + 2*pi*(Theta<0);


R = sqrt(X.^2 + Y.^2);


end