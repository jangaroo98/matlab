function laplace_curvilinear(N1,N2,w,tol,region)

%solves the helmholts equation laplacian(u) = 0 on the domain region
%in curvilinear coordinates.



[X,Y] = elliptic_polar(N1,N2,w,tol,5000,region);
size = (N1-1)*N2;
rhs = zeros(N1-1,N2);
A = spalloc(size,size,3*size);



for i=1:N1-1
    for j=1:N2
        spot = (N1-1)*(j-1) + i;
        if j == 1
            A(spot,spot) = 1;
            rhs(i,j) =  3;%-uinc_hat(i); % inner boundary condition
            
            
        elseif j == N2
            A(spot,spot) = 1;
            rhs(i,j) = 0; %outer boundary condition
        elseif i == 1
            xe = 0.5*(X(i+1,j) - X(N1-1,j));
            xn = 0.5*(X(i,j+1) - X(i,j-1));
            ye = 0.5*(Y(i+1,j) - Y(N1-1,j));
            yn = 0.5*(Y(i,j+1) - Y(i,j-1));

            xne = 0.25*(X(N1-1,j-1)+X(i+1,j+1)-X(i+1,j-1)-X(N1-1,j+1));
            yne = 0.25*(Y(N1-1,j-1)+Y(i+1,j+1)-Y(i+1,j-1)-Y(N1-1,j+1));
            
            alpha = xn^2 + yn^2;
            beta = xe*xn + ye*yn;
            gamma = xe^2 + ye^2;
            J = xe*yn - xn*ye;
            ae = 2*(xn*xne + yn*yne);%doesn't have 2?
            gn = 2*(xe*xne + ye*yne);%doesn't have 2?
            
            A(spot,spot - 1) = -0.5*beta/(J^2);
            A(spot,spot - (N1-1)) = gamma/(J^2) - gn/(4*J^2); %%%%%2??? had 4
            A(spot,spot - (N1-1) + 1) = 0.5*beta/(J^2);

            A(spot,spot-1 + N2) = alpha/(J^2) - ae/(4*J^2); %%%%%2??? had 4
            A(spot,spot) = -2*(gamma/(J^2)) - 2*alpha/(J^2);
            A(spot,spot+1) = alpha/(J^2) + ae/(4*J^2);   %%%%%2??? had 4

            A(spot,spot + 2*(N1-1) - 1) = 0.5*beta/(J^2);
            A(spot,spot + (N1-1)) = gamma/(J^2) + gn/(4*J^2); %2? had 4
            A(spot,spot + (N1-1) + 1) = -0.5*beta/(J^2);
            
        elseif i == N1-1
            
            xe = 0.5*(X(1,j) - X(i-1,j));
            xn = 0.5*(X(i,j+1) - X(i,j-1));
            ye = 0.5*(Y(1,j) - Y(i-1,j));
            yn = 0.5*(Y(i,j+1) - Y(i,j-1));

            xne = 0.25*(X(i-1,j-1)+X(1,j+1)-X(1,j-1)-X(i-1,j+1));
            yne = 0.25*(Y(i-1,j-1)+Y(1,j+1)-Y(1,j-1)-Y(i-1,j+1));

            alpha = xn^2 + yn^2;
            beta = xe*xn + ye*yn;
            gamma = xe^2 + ye^2;
            J = xe*yn - xn*ye;
            ae = 2*(xn*xne + yn*yne);%doesn't have 2?
            gn = 2*(xe*xne + ye*yne);%doesn't have 2?


            A(spot,spot - (N1-1) - 1) = -2*beta/(4*J^2);
            A(spot,spot - (N1-1)) = gamma/(J^2) - gn/(4*J^2);
            A(spot,spot - 2*(N1-1) + 1) = 2*beta/(4*J^2);

            A(spot,spot-1) = alpha/(J^2) - ae/(4*J^2); 
            A(spot,spot) = -2*(gamma/(J^2)) - 2*alpha/(J^2);
            A(spot,spot -(N1-1) +1) = alpha/(J^2) + ae/(4*J^2);

            A(spot,spot + (N1-1) - 1) = 2*beta/(4*J^2);
            A(spot,spot + (N1-1)) = gamma/(J^2) + gn/(4*J^2);
            A(spot,spot + 1) = -2*beta/(4*J^2);
        
        
        
        
        else
            xe = 0.5*(X(i+1,j) - X(i-1,j));
            xn = 0.5*(X(i,j+1) - X(i,j-1));
            ye = 0.5*(Y(i+1,j) - Y(i-1,j));
            yn = 0.5*(Y(i,j+1) - Y(i,j-1));

            xne = 0.25*(X(i-1,j-1)+X(i+1,j+1)-X(i+1,j-1)-X(i-1,j+1));
            yne = 0.25*(Y(i-1,j-1)+Y(i+1,j+1)-Y(i+1,j-1)-Y(i-1,j+1));

            alpha = xn^2 + yn^2;
            beta = xe*xn + ye*yn;
            gamma = xe^2 + ye^2;
            J = xe*yn - xn*ye;
            ae = 2*(xn*xne + yn*yne); %doesn't have 2?
            gn = 2*(xe*xne + ye*yne); %doesn't have 2?


            A(spot,spot - (N1-1) - 1) = -2*beta/(4*J^2);
            A(spot,spot - (N1-1)) = gamma/(J^2) - gn/(4*J^2);
            A(spot,spot - (N1-1) + 1) = 2*beta/(4*J^2);

            A(spot,spot-1) = alpha/(J^2) - ae/(4*J^2); 
            A(spot,spot) = -2*(gamma/(J^2)) - 2*alpha/(J^2);
            A(spot,spot+1) = alpha/(J^2) + ae/(4*J^2);

            A(spot,spot + (N1-1) - 1) = 2*beta/(4*J^2);
            A(spot,spot + (N1-1)) = gamma/(J^2) + gn/(4*J^2);
            A(spot,spot + (N1-1) + 1) = -2*beta/(4*J^2);
        
        end
    end
end




%full(A)



b = reshape(rhs,size,1);
u = A\b;
u_soln = reshape(u,N1-1,N2);
u_soln = [u_soln ; u_soln(1,:)];
clf
surface(X,Y,abs(u_soln))
 


end