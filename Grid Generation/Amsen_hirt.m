N1 = 41; %eida
N2 = 61; %nu
maxiter = 1000;
tol = 1e-5;
[BX,BY,simplyconnected] = boundary_of_region(N1,N2,'swan');

w = 4/(2+sqrt(4-(cos(pi/N1)+cos(pi/N2))^2));

[IX,IY] = interior_grid(N1,N2);
BX(2:N1-1,2:N2-1) = IX;
BY(2:N1-1,2:N2-1) = IY;
u = zeros(N1,N2);
mesh(BX,BY,u)
pause()

x_old = BX;
y_old = BY;

x_new = BX;
y_new = BY;



for k=1:maxiter
    for i=2:N1-1
        for j=2:N2-1
            x_hatij = 0.25*(x_new(i-1,j)+x_old(i+1,j)+x_old(i,j+1)+x_new(i,j-1));
            y_hatij = 0.25*(y_new(i-1,j)+y_old(i+1,j)+y_old(i,j+1)+y_new(i,j-1));
            x_new(i,j) = w*x_hatij + (1-w)*x_old(i,j);
            y_new(i,j) = w*y_hatij + (1-w)*y_old(i,j);
        end
    end
    diffx = x_new - x_old;
    diffy = y_new - y_old;
    bool1 = norm(diffx,Inf) < tol;
    bool2 = norm(diffy,Inf) < tol;
    if (bool1 && bool2)
        fprintf('converged %d',k)
        break
    end
    x_old = x_new;
    y_old = y_new;
    mesh(x_new,y_new,u)
    pause(0.01)
end

mesh(x_new,y_new,u)




