%script that will run code for the annulus, asteroid and leaf regions. It

PPWV = [30,40,50,60];
k = 2*pi;
r0 = 1;
L = 10;
w = 1.8;
tol = 1e-5;
uscexact_fun = ExactSoln(20,r0);


ntest = length(PPWV);
dthetaV = zeros(ntest,1);
maxerrorFFPV = zeros(ntest,1);
L2errorFFPV = zeros(ntest,1);
RelL2errorFFPV = zeros(ntest,1);
RelmaxerrorFFPV = zeros(ntest,1);
RelmaxerrorV = zeros(ntest,1);
L2errorV = zeros(ntest,1);
RelL2errorV = zeros(ntest,1);
maxerrorV = zeros(ntest,1);

for jtest = 1:ntest
    PPW = PPWV(jtest);
%     figure
    [X,Y,usc_approx,uinc_eval,ffp,dt] = Helmholts_curvilinear_abc(PPW,L,w,tol,'annu3',false);
    
    R = sqrt(X.^2 + Y.^2);
    T = to_polar(X,Y);
    [N1,N2] = size(X);
    A0 = ExactSolnFarFieldPatt2D(15,N1,N2);
    uscex = uscexact_fun(T,R);
    utotex = uscex + uinc_eval;
    utot_approx = usc_approx + uinc_eval;
    
  
    v1 = reshape(utot_approx,1,[]);
    v2 = reshape(utotex,1,[]);
    
    maxerrorV(jtest) = norm(v1-v2,inf);
    L2errorV(jtest) = norm(v1-v2);
    maxerrorFFPV(jtest) = norm(abs(A0) - abs(ffp),inf);
    L2errorFFPV(jtest) =norm(abs(A0)-abs(ffp));
    dthetaV(jtest) = dt;
end

Helmholts_curvilinear_abc(30,L,w,tol,'roid3',true);


Helmholts_curvilinear_abc(30,L,w,tol,'leaf3',true);

figure 
polarplot(T(:,N2),abs(ffp),'r*') 
hold on
polarplot(T(:,N2),abs(A0))
hold off


figure
loglog(dthetaV,maxerrorV,'o-') 
title('Error Plot of Computed Scattering')
xlabel('delta theta')
ylabel('Infinity Norm error')

figure
loglog(dthetaV,maxerrorFFPV,'o-')
title('Error plot of computed far field pattern')

fprintf('Error table for computed scattering')
error_table(dthetaV,maxerrorV)

fprintf('Error table for computed far field pattern')
error_table(dthetaV,maxerrorFFPV)

figure
surface(X,Y,abs(utotex)) 
title(['Exact Solution, PPW = ' num2str(PPWV(ntest))])
shading interp;
figure 
surface(X,Y,abs(utot_approx)) 
title(['Approximated Solution, PPW = ' num2str(PPWV(ntest))])
shading interp;



function theta = to_polar(X,Y)

theta = zeros(size(X),'like',X);
[n,m] = size(X);
for i=1:n
    for j=1:m
        x = X(i,j);
        y = Y(i,j);
        thet = atan(y/x);
        if x<0
            thet = thet + pi;
        end
        theta(i,j) = thet;
    end
end

end
function uscex = ExactSoln(NFS,r_0)
k = 2*pi;
epsilon_0 = 1;
epsilon_n = 2;
first_term = @(theta,r) besselj(0,k*r_0) .* besselh(0,k*r) ./ besselh(0,k*r_0);
single_expression = @(n,theta,r) epsilon_n * ((1j)^n) * besselj(n,k*r_0) * besselh(n,k*r) * cos(n*theta) / besselh(n,k*r_0);

uscex = @(theta,r) -first_term(theta,r) - summing(NFS,r_0,r,theta,k);
end
function value = summing(NFS,r_0,r,theta,k)
tracker = zeros(size(theta),'like',theta);
for n = 1:NFS
    tracker = tracker + 2 * ((1j)^n) .* besselj(n,k*r_0) .* besselh(n,k*r) .* cos(n*theta) ./ besselh(n,k*r_0);
end
value = tracker;
end
 

