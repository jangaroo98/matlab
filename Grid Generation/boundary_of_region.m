function  [BX,BY,simplyconnected] = boundary_of_region(N1,N2,region)
%This function gives the boundary of a region that is specified. 
%N1 = number of points in x or theta
%N2 = number of points in y or radial

%if the region is simply connected, the boundary has to be described by 4
%parametric equations for the 4 boundaries. 

%if the region is multiply connected, then the boundary needs to be
%described by 2 functions in polar coordinates

%This function returns the boundary Bx, BY, as well as a bool
%simplyconnected that is true if the region is simply connected. 


if region == 'swan0'
    bbx = @(t) t;%bottom boundary of region
    bby = @(t) 0;
    btx = @(t) t; %top boundary of region
    bty = @(t) 1-2*t + 2*(t.^2);
    blx = @(t) 0; % left boundary of region
    bly = @(t) t;
    brx = @(t) 1+2*t-2*t.^2; %right boundary
    bry = @(t) t;
    simplyconnected=true;
elseif region == 'dome0'
    bbx = @(t) t;%bottom boundary of region
    bby = @(t) 0;
    btx = @(t) t; %top boundary of region
    bty = @(t) -4*(t-.5)^2 + 2;
    blx = @(t) 0; % left boundary of region
    bly = @(t) t;
    brx = @(t) 1; %right boundary
    bry = @(t) t;
    simplyconnected=true;
elseif region == 'annu3'
    bbx = @(t) cos(t);%bottom boundary of region
    bby = @(t) sin(t);
    btx = @(t) 3*cos(t); %top boundary of region
    bty = @(t) 3*sin(t);
    simplyconnected=false;
elseif region == 'annu2'
    bbx = @(t) cos(t);%bottom boundary of region
    bby = @(t) sin(t);
    btx = @(t) 2*cos(t); %top boundary of region
    bty = @(t) 2*sin(t);
    simplyconnected=false;
elseif region == 'leaf3'
    bbx = @(t) 1.5*0.3*(2 + cos(3*t))*cos(t);%bottom boundary of region
    bby = @(t) 1.5*0.3*(2 + cos(3*t))*sin(t);
    btx = @(t) 3*cos(t); %top boundary of region
    bty = @(t) 3*sin(t);
    simplyconnected=false;
elseif region == 'roid3'
    bbx = @(t) 0.25*(3*cos(t) + cos(3*t));%bottom boundary of region
    bby = @(t) 0.25*(3*sin(t) - sin(3*t));
    btx = @(t) 3*cos(t); %top boundary of region
    bty = @(t) 3*sin(t);
    simplyconnected=false;
end

if simplyconnected 
%the boundary is generated differently depending on if it is simply
%connected or not
    ti = linspace(0,1,N1);
    tj = linspace(0,1,N2);

    BX = zeros(N1,N2); %initialize BX,BY
    BY = zeros(N1,N2);
    for i=1:N1
        BX(i,1) = bbx(ti(i)); %fill bottom boundary
        BY(i,1) = bby(ti(i)); 

        BX(i,N2) = btx(ti(i)); %fill top boundary
        BY(i,N2) = bty(ti(i));
    end
    for j=1:N2
        BX(1,j) = blx(tj(j)); %fill left boundary
        BY(1,j) = bly(tj(j));

        BX(N1,j) = brx(tj(j)); %fill right boundary
        BY(N1,j) = bry(tj(j));
    end


%     clf
%     hold on
%     plot(BX(1,:),BY(1,:))
%     plot(BX(N1,:),BY(N1,:))
%     plot(BX(:,1),BY(:,1))
%     plot(BX(:,N2),BY(:,N2))
%     hold off
else %if the region is multiply connected
    BX = zeros(N1,N2);
    BY = zeros(N1,N2);
    theta=fliplr(linspace(0,2*pi,N1));
    for i=1:N1
        BX(i,1) = bbx(theta(i)); %fill boundary 
        BY(i,1) = bby(theta(i));
        BX(i,N2) = btx(theta(i));
        BY(i,N2) = bty(theta(i));
    end
%     clf
%     hold on
%     plot(BX(:,1),BY(:,1))
%     plot(BX(:,N2),BY(:,N2))
%     hold off
end
end
